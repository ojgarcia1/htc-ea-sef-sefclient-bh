﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.DTE
{
    public class SelfClienteResponse
    {
        public int? version { get; set; }
        public string? ambiente { get; set; }
        public int? versionApp { get; set; }
        public string? estado { get; set; }
        public string? codigoGeneracion { get; set; }
        public string? numeroControl { get; set; }
        public string? selloRecibido { get; set; }
        public string? fhProcesamiento { get; set; }
        public string? codigoMsg { get; set; }
        public string? descripcionMsg { get; set; }
        public string[]? observaciones { get; set; }

        public string? digital { get; set; }

        /* public string resolucion { get; set; }
         public string numSerie { get; set; }
         public int numFiscal { get; set; }
         public string fechaProceso { get; set; }
         public string horaProceso { get; set; }      
         public string numDocumento { get; set; }*/


    }
}
