﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.DTE
{
    public class dte_fac
    {
        public class Rootobject
        {
            public Identificacion identificacion { get; set; }
            public DocumentoRelacionado? documentoRelacionado { get; set; }
            public Emisor emisor { get; set; }
            public Receptor receptor { get; set; }
            public OtrosDocumentos? otrosDocumentos { get; set; }
            public VentaTercero? ventaTercero { get; set; }
            public CuerpoDocumento[] cuerpoDocumento { get; set; }
            public Resumen resumen { get; set; }
            public Extension extension { get; set; }
            public Apendice apendice { get; set; }

        }

        public class Identificacion
        {
            public int version { get; set; }
            public string ambiente { get; set; }
            public string tipoDte { get; set; }
            public string? numeroControl { get; set; }
            public string codigoGeneracion { get; set; }
            public int tipoModelo { get; set; }
            public int tipoOperacion { get; set; }
            public string? tipoContingencia { get; set; }
            public string? motivoContin { get; set; }
            public string fecEmi { get; set; }
            public string? horEmi { get; set; }
            public string tipoMoneda { get; set; }
            //invalidacion
            //public string? fecAnula { get; set; }
            // public string? horAnula { get; set; }

        }

        public class DocumentoRelacionado
        {
            public string? tipoDocumento { get; set; }
            public int? tipoGeneracion { get; set; }
            public string? numeroDocumento { get; set; }
            public string? fechaEmision { get; set; }
        }

        public class Emisor
        {
            public string code { get; set; }
            //public string? codeSucursal { get; set; }
            public string? nit { get; set; }
            public string? nrc { get; set; }
            public string? nombre { get; set; }
            public string? codActividad { get; set; }
            public string? descActividad { get; set; }
            public string? nombreComercial { get; set; }
            public string? tipoEstablecimiento { get; set; }
            public Direccion direccion { get; set; }
            public string? telefono { get; set; }
            public string? correo { get; set; }
            //invalidacion
            public string? codPuntoVentaMH { get; set; }
        }

        public class Receptor
        {
            //public string nit { get; set; }
            public string nrc { get; set; }
            public string nombre { get; set; }
            public string? tipoDocumento { get; set; }
            public string? numDocumento { get; set; }
            public string codActividad { get; set; }
            public string descActividad { get; set; }
            // public string nombreComercial { get; set; }
            public Direccion direccion { get; set; }
            public string telefono { get; set; }
            public string correo { get; set; }
            //   public string? codPais { get; set; }
            // public string? nombrePais { get; set; }
            // public int? tipoPersona { get; set; }
            //  public string? bienTitulo { get; set; }
        }

        public class Direccion
        {
            public string departamento { get; set; }
            public string municipio { get; set; }
            public string complemento { get; set; }
        }

        public class CuerpoDocumento
        {
            public int numItem { get; set; }
            public int tipoItem { get; set; }
            public string? numeroDocumento { get; set; }
            public string? codigo { get; set; }
            public string? codTributo { get; set; }
            public string? descripcion { get; set; }
            public int cantidad { get; set; }
            public int? uniMedida { get; set; }
            public decimal precioUni { get; set; }
            public decimal montoDescu { get; set; }
            public decimal ventaNoSuj { get; set; }
            public decimal ventaExenta { get; set; }
            public decimal ventaGravada { get; set; }
            public decimal psv { get; set; }
            public decimal noGravado { get; set; }
            //public string tipoDte { get; set; }
            //public int tipoGeneracion { get; set; }
            //public string fechaGeneracion { get; set; }
            //public decimal exportaciones { get; set; }
            public int[]? tributos { get; set; }
            public decimal ivaItem { get; set; }
            //public string? obsItem { get; set; }
            //public int tipoDoc { get; set; }
            //public string? numDocumento { get; set; }
            //public string? fechaEmision { get; set; }
            //public decimal? montoSujetoGrav { get; set; }
            //public string? codigoRetencionMH { get; set; }
            //public decimal? ivaRetenido { get; set; }
        }

        public class Resumen
        {
            public decimal totalNoSuj { get; set; }
            public decimal totalExenta { get; set; }
            public decimal totalGravada { get; set; }
            //public decimal totalExportacion { get; set; }
            public decimal subTotalVentas { get; set; }
            public decimal descuNoSuj { get; set; }
            public decimal descuExenta { get; set; }
            public decimal descuGravada { get; set; }
            public decimal porcentajeDescuento { get; set; }
            public decimal totalDescu { get; set; }
            public Tributos tributos { get; set; }
            public decimal subTotal { get; set; }
            //public decimal ivaPerci1 { get; set; }
            public decimal ivaRete1 { get; set; }
            public decimal reteRenta { get; set; }
            //public decimal total { get; set; }
            public decimal montoTotalOperacion { get; set; }
            public decimal totalNoGravado { get; set; }
            public decimal totalPagar { get; set; }
            public string? totalLetras { get; set; }
            public decimal totalIva { get; set; }
            public decimal saldoFavor { get; set; }
            public int condicionOperacion { get; set; }
            public Pagos[] pagos { get; set; }
            //public string? codIncoterms { get; set; }
            //public string? descIncoterms { get; set; }
            //public string? observaciones { get; set; }
            public string? numPagoElectronico { get; set; }
            //public decimal descuento { get; set; }
            // public decimal totalSujetoRetencion { get; set; }
            //public decimal totalIVAretenido { get; set; }
        }

        public class Tributos
        {
            public string codigo { get; set; }
            public string descripcion { get; set; }
            public decimal valor { get; set; }
        }

        public class Extension
        {
            public string? nombEntrega { get; set; }
            public string? docuEntrega { get; set; }
            public string? nombRecibe { get; set; }
            public string? docuRecibe { get; set; }
            public string? observaciones { get; set; }
            public string? placaVehiculo { get; set; }
        }

        public class Apendice
        {
            public string campo { get; set; }
            public string etiqueta { get; set; }
            public string valor { get; set; }
        }

        public class OtrosDocumentos
        {
            public string? codDocAsociado { get; set; }
            public string? descDocumento { get; set; }
            public string? detalleDocumento { get; set; }
            public Medico? medico { get; set; }
        }

        public class Medico
        {
            public string? nit { get; set; }
            public string? docIdentificacion { get; set; }
            public int? tipoServicio { get; set; }
        }

        public class VentaTercero
        {
            public string? nit { get; set; }
            public string? nombre { get; set; }
        }

        public class Pagos
        {
            public string? codigo { get; set; }
            public string? mntoPago { get; set; }
            public string? referencia { get; set; }
            public string? plazo { get; set; }
            public string? periodo { get; set; }
        }
    }
}
