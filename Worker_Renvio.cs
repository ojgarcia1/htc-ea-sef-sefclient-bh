﻿using App.WindowsService.log;
using App.WindowsService.log.LogWriter;
using App.WindowsService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService
{
    public class Worker_Renvio : BackgroundService
    {
        private readonly ISEFClientService _SEFClientService;
        private readonly ILogger<Worker_Renvio> _logger;
        private readonly DailyLogFileWriter _fileLogger;
        private string _modulo = "reenvio";
        public Worker_Renvio(ISEFClientService sefclientservice, ILogger<Worker_Renvio> logger)
        {
            _logger = logger;
            _SEFClientService = sefclientservice;
            string logDirectory = "/var/log/"; // Reemplaza con la ruta real de tu directorio de logs
            _fileLogger = new DailyLogFileWriter(logDirectory);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _fileLogger.Log("Worker_Renvio running at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
                _logger.LogInformation("Worker_Renvio running at: {time}", DateTimeOffset.Now);
                await _SEFClientService.LeerDocumentosReenvioAsync(stoppingToken);
                // await Task.Delay(1000, stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Worker_Renvio Started at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Worker_Renvio Services Started: {time}", DateTimeOffset.Now);
            return base.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Worker_Renvio Stopped at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Worker_Renvio Services Stopped: {time}", DateTimeOffset.Now);
            return base.StopAsync(stoppingToken);
        }
    }
}
