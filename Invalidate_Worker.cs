﻿using App.WindowsService.log;
using App.WindowsService.log.LogWriter;
using App.WindowsService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WSIntegracionSEF;

namespace App.WindowsService
{
    public class Invalidate_Worker : BackgroundService
    {
        private readonly ISEFClientService _SEFClientService;
        private readonly ILogger<Invalidate_Worker> _logger;
        //private readonly FileLogger _fileLogger;
        private readonly DailyLogFileWriter _fileLogger;
        private static bool _isRunning = false;
        private static readonly object _lockObject = new object();
        private string _modulo = "Invalidacion";

        public Invalidate_Worker(ISEFClientService sefclientservice, ILogger<Invalidate_Worker> logger)
        {
            _logger = logger;
            _SEFClientService = sefclientservice;
            string logDirectory = "/var/log/"; // Reemplaza con la ruta real de tu directorio de logs
            _fileLogger = new DailyLogFileWriter(logDirectory);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (!TryAcquireLock())
            {
                // Si el bloqueo no se pudo adquirir, el servicio ya se está ejecutando
                _fileLogger.Log("El Worker Service ya está en ejecución.", _modulo);
                _logger.LogInformation("El Worker Service ya está en ejecución.");
                return;
            }

            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    _fileLogger.Log("Invalidate_Worker running at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
                    _logger.LogInformation("Invalidate_Worker running at: {time}", DateTimeOffset.Now);
                    await _SEFClientService.LeerDocumentosAnuladosAsync(stoppingToken);
                }

            }
            finally
            {
                ReleaseLock(); // Liberamos el bloqueo al finalizar la ejecución
            }

        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Invalidate_Worker Started at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Invalidate_Worker Services Started: {time}", DateTimeOffset.Now);
            return base.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Invalidate_Worker Stopped at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Invalidate_Worker Services Stopped: {time}", DateTimeOffset.Now);
            return base.StopAsync(stoppingToken);
        }

        private bool TryAcquireLock()
        {
            lock (_lockObject)
            {
                if (_isRunning)
                {
                    _fileLogger.Log("El servicio ya se está ejecutando", _modulo);
                    return false; // El servicio ya se está ejecutando
                }

                _isRunning = true;
                _fileLogger.Log("El bloqueo se adquirió correctamente", _modulo);
                return true; // El bloqueo se adquirió correctamente
            }
        }

        private void ReleaseLock()
        {
            lock (_lockObject)
            {
                _fileLogger.Log("Liberamos el bloqueo", _modulo);
                _isRunning = false; // Liberamos el bloqueo
            }
        }
    }
}
