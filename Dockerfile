#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:6.0 AS base
WORKDIR /app

# Configura las variables de entorno de cadena de conexi�n de la base de datos
ENV ACCEPT_EULA=Y
ENV ConnectionStrings__DbPassword="********"
ENV ConnectionStrings__DbHost="*********"
ENV ConnectionStrings__DbPort="c"
ENV ConnectionStrings__DbSERVICENAME="d"
ENV ConnectionStrings__DbUserId="e"
ENV TZ=America/El_Salvador
ENV ConnectionStrings__ASPNETCORE_ENVIRONMENT="Development"
#ENV ConnectionStrings__ASPNETCORE_ENVIRONMENT="Production"
ENV ConnectionStrings__Esquema="****"
ENV ConnectionStrings__QueryOnDemand="Sqlselectdoc"
ENV ConnectionStrings__QueryInvalidate="Sqlselectdocanulado"
ENV ConnectionStrings__QueryContingency="SqlselectContingencydoc"
ENV ConnectionStrings__QueryRetrive="SqlDocRetrives"
ENV ConnectionStrings__QueryDatosContingencia="SqlQueryDatosContingencia"



FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["WSIntegracionSEF.csproj", "."]
RUN dotnet restore "./WSIntegracionSEF.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "WSIntegracionSEF.csproj" -c Release -o /app/build


FROM build AS publish
RUN dotnet publish "WSIntegracionSEF.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "WSIntegracionSEF.dll"]