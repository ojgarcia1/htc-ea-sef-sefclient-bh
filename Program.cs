using App.WindowsService;
using App.WindowsService.Services;
using System.Diagnostics;

namespace WSIntegracionSEF
{
    public class Program
    {
        public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args)
               .ConfigureLogging(logging =>
               {
                   logging.ClearProviders();
                   logging.AddConsole();
                   //logging.AddEventLog();

               })
              .ConfigureAppConfiguration((hostingContext, config) =>
              {
                  //config.AddJsonFile("appsettings.json", optional: true);
                  config.AddEnvironmentVariables(prefix: "ConnectionStrings__");
              })
              // Essential to run this as a window service
              .UseWindowsService()
              .UseSystemd()
              .ConfigureServices(configureServices);

        private static void configureServices(HostBuilderContext context, IServiceCollection services)
        {
            services.AddMemoryCache();
            //services.Configure<SEFClientconfig>(context.Configuration.GetSection("SEFClient"));
            services.AddLogging();
            services.AddSingleton<ISEFClientService, SEFClientService>();
            services.AddHostedService<Worker>();
            services.AddHostedService<Contingency_Worker>();
            services.AddHostedService<Invalidate_Worker>();
            services.AddHostedService<Worker_Renvio>();
        }
    }
}