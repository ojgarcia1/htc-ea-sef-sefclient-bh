using App.WindowsService.Services;
using System.Diagnostics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using App.WindowsService.log;
using App.WindowsService.log.LogWriter;
using System.Reflection.Metadata;
using System.Reflection;

namespace WSIntegracionSEF
{
    public class Worker : BackgroundService
    {
        private readonly ISEFClientService _SEFClientService;
        private readonly ILogger<Worker> _logger;
        private readonly DailyLogFileWriter _fileLogger;
        private static bool _isRunning = false;
        private static readonly object _lockObject = new object();
        private string _modulo = "onDemand";

        public Worker(ISEFClientService sefclientservice, ILogger<Worker> logger)
        {
            _logger = logger;
            _SEFClientService = sefclientservice;
            string logDirectory = "/var/log/"; // Reemplaza con la ruta real de tu directorio de logs
            _fileLogger = new DailyLogFileWriter(logDirectory);

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (!TryAcquireLock())
            {
                // Si el bloqueo no se pudo adquirir, el servicio ya se est� ejecutando
                _fileLogger.Log("El Worker Service ya est� en ejecuci�n.", _modulo);
                _logger.LogInformation("El Worker Service ya est� en ejecuci�n.");
                return;
            }

            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    _fileLogger.Log("Worker running at: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    await _SEFClientService.LeerDocumentosAsync(stoppingToken);
                }

            }
            finally
            {
                ReleaseLock(); // Liberamos el bloqueo al finalizar la ejecuci�n
            }
        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Worker Services Started: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Worker Services Started: {time}", DateTimeOffset.Now);
            return base.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            _fileLogger.Log("Worker Services Stopped: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss"), _modulo);
            _logger.LogInformation("Worker Services Stopped: {time}", DateTimeOffset.Now);
            return base.StopAsync(stoppingToken);
        }

        private bool TryAcquireLock()
        {
            lock (_lockObject)
            {
                if (_isRunning)
                {
                    _fileLogger.Log("El servicio ya se est� ejecutando", _modulo);
                    return false; // El servicio ya se est� ejecutando
                }

                _isRunning = true;
                _fileLogger.Log("El bloqueo se adquiri� correctamente", _modulo);
                return true; // El bloqueo se adquiri� correctamente
            }
        }

        private void ReleaseLock()
        {
            lock (_lockObject)
            {
                _fileLogger.Log("Liberamos el bloqueo", _modulo);
                _isRunning = false; // Liberamos el bloqueo
            }
        }
    }
}