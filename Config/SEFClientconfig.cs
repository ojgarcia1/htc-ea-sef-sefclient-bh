﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.Config
{
    public class SEFClientconfig
    {
        public int PeriodoMinutos { get; set; }
        public string SqlConnStr { get; set; }
        public string Sqlselectdoc { get; set; }
        public string Sqlselectdoc_detalle { get; set; }
        public string Sqlselec_resumen { get; set; }
        public string Sqlselectdocanulado { get; set; }
        public string Sqlselectdoccontoingencia { get; set; }
        public string Sqlupdatestr { get; set; }
        public string url_reception { get; set; }
        public string url_base { get; set; }
        public string url_security { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string OracleConnStr { get; set; }

        public string esquema { get; set; }


    }
}
