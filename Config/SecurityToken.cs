﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.Config
{
    public class SecurityToken
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
