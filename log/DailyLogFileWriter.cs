﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.log
{
    using System;
    using System.IO;

    namespace LogWriter
    {
        public class DailyLogFileWriter
        {
            private readonly string logDirectory;
            private StreamWriter logWriter;

            public DailyLogFileWriter(string logDirectory)
            {
                this.logDirectory = logDirectory;
            }

            public void Log(string message, string modulo)
            {
                EnsureLogFileIsOpen(modulo);

                logWriter.WriteLine($"Modulo: {modulo} Mensaje: {DateTime.Now}: {message}");
                logWriter.Flush();
            }

            private void EnsureLogFileIsOpen(string modulo)
            {
                string logFilePath = GetLogFilePath(modulo);

                if (logWriter == null || logWriter.BaseStream == null || logWriter.BaseStream.CanWrite == false)
                {
                    CloseLogFile();

                    logWriter = new StreamWriter(logFilePath, append: true);
                }
            }

            private string GetLogFilePath(string modulo)
            {
               // string logFileName = $"sefclient_{DateTime.Now:yyyy-MM-dd}.log";
                string logFileName = $"{modulo}_{DateTime.Now:yyyy-MM-dd}.log";
                string logFilePath = Path.Combine(logDirectory, logFileName);
                return logFilePath;
            }

            private void CloseLogFile()
            {
                if (logWriter != null)
                {
                    logWriter.Close();
                    logWriter.Dispose();
                    logWriter = null;
                }
            }
        }
    }

}
