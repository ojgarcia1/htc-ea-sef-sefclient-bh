﻿using App.WindowsService.Config;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using App.WindowsService.log;
using App.WindowsService.log.LogWriter;
using Azure;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Json;
using App.WindowsService.DTE;
using System.Diagnostics;

namespace App.WindowsService.Services
{
    public class SEFClientService : ISEFClientService
    {
        private readonly ILogger<SEFClientService> _logger;
        private readonly IMemoryCache _cache;
        private readonly IConfiguration _envconfig;
        private readonly DailyLogFileWriter _fileLogger;

        public SEFClientService(IOptions<SEFClientconfig> config, ILogger<SEFClientService> logger, IMemoryCache cache, IConfiguration envconfig)
        {
            _logger = logger;
            _cache = new MemoryCache(new MemoryCacheOptions());
            _envconfig = envconfig;
            string logDirectory = "/var/log/"; // Reemplaza con la ruta real de tu directorio de logs
            _fileLogger = new DailyLogFileWriter(logDirectory);
        }

        public async Task LeerDocumentosAsync(CancellationToken stoppingToken)
        {

            #region variables
            /*********************************************Declaracion de Variables***************************************************/
            SEFClientFunctions func = new SEFClientFunctions();
            string jsonstr = string.Empty;
            string jsonstr_apendices = string.Empty;
            dynamic do_json = new JObject();
            dynamic tk = new JObject();
            string token = string.Empty;
            string respservicio = string.Empty;
            DataTable dtdoc = new DataTable();
            DataTable dt_config = new DataTable();
            DataTable dtdetalledoc = new DataTable();
            DataTable dtresumen = new DataTable();
            DataTable dt_apendices = new DataTable();
            DataView dv = new DataView();
            DataRow dr;
            int idrow = 0;
            string estado_servicio = string.Empty;
            Config.SecurityToken securityToken = new Config.SecurityToken();
            string JSONST = string.Empty;
            string JConfigString = string.Empty;
            string url_reception = string.Empty;
            string url_security = string.Empty;
            string url_base = string.Empty;
            string Col_IdDocEnc = string.Empty;
            string Sqlselectdoc = string.Empty;
            string MHresponse = string.Empty;
            string Col_TipoDTE = string.Empty;
            string Tbl_DocEncabezado = string.Empty;
            string Col_resp = string.Empty;
            string Tbl_log = string.Empty;
            string Col_Estado = string.Empty;
            string Col_sello = string.Empty;
            string Col_UidDocEnc = string.Empty;
            string Col_Reintentos = string.Empty;
            int PeriodoMinutos = 0;
            string Col_TotalVentaGravada = string.Empty;
            string OracleConnStr = string.Empty;
            string esquema = string.Empty;
            int reintentos = 0;
            int Num_Reintentos = 0;
            string Tbl_sefclientconf = string.Empty;
            string Coldtetype_config = string.Empty;
            string SqlApendices = string.Empty;
            string estado_servicio_reception = string.Empty;
            string SqlUpdateContingencia = string.Empty;
            string querytouse = string.Empty;
            string num_doc_relacionado = string.Empty;
            string modulo = string.Empty;
            string estado = string.Empty;
            SelfClienteResponse nullresponse = new SelfClienteResponse();
            string ambiente = string.Empty;
            #endregion

            modulo = "onDemand";

            var stopwatch = new Stopwatch();
            var stopwatch_dataset = new Stopwatch();
            var stopwatch_row = new Stopwatch();
            stopwatch.Start();

            #region contenedor_enviroment_variables
            _logger.LogInformation("DbUserId: " + _envconfig.GetConnectionString("DbUserId"));
            _fileLogger.Log("DbUserId: " + _envconfig.GetConnectionString("DbUserId"), modulo);
            _logger.LogInformation("DbHost: " + _envconfig.GetConnectionString("DbHost"));
            _fileLogger.Log("DbHost: " + _envconfig.GetConnectionString("DbHost"), modulo);
            _logger.LogInformation("DbPort: " + _envconfig.GetConnectionString("DbPort"));
            _fileLogger.Log("DbPort: " + _envconfig.GetConnectionString("DbPort"), modulo);
            _logger.LogInformation("DbSERVICENAME: " + _envconfig.GetConnectionString("DbSERVICENAME"));
            _fileLogger.Log("DbSERVICENAME: " + _envconfig.GetConnectionString("DbSERVICENAME"), modulo);
            _logger.LogInformation("SQLDOC: " + _envconfig.GetConnectionString("SQLDOC"));
            _fileLogger.Log("SQLDOC: " + _envconfig.GetConnectionString("SQLDOC"), modulo);
            _logger.LogInformation("QueryOnDemand: " + _envconfig.GetConnectionString("QueryOnDemand"));
            _fileLogger.Log("QueryOnDemand: " + _envconfig.GetConnectionString("QueryOnDemand"), modulo);
            #endregion

            #region conexiondb
            if (_envconfig.GetConnectionString("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                //OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 192.168.139.4)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = XE)));User ID=BHDTE;Password=Sefclient123;";
                OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 192.168.139.4)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = XE)));User ID=BHDTE;Password=Sefclient123;Pooling=False;";
                esquema = "BHDTE";
                _fileLogger.Log("OracleConnStr: " + OracleConnStr, modulo);
                _logger.LogInformation("OracleConnStr: " + OracleConnStr);
            }
            else
            {
                OracleConnStr = func.OracleConnStr(_envconfig.GetConnectionString("DbHost"), _envconfig.GetConnectionString("DbPort"), _envconfig.GetConnectionString("DbSERVICENAME"), _envconfig.GetConnectionString("DbUserId"), _envconfig.GetConnectionString("DbPassword"));
                esquema = _envconfig.GetConnectionString("Esquema");
            }

            _logger.LogInformation("ConexionDB: " + func.TestConexionDB(OracleConnStr));
            _fileLogger.Log("ConexionDB: " + func.TestConexionDB(OracleConnStr), modulo);
            //_cache.Remove(dt_config);
            #endregion


            try
            {

                _fileLogger.Log("************************inicio funcion LeerDocumentosAsync**************************************", modulo);
                _fileLogger.Log("Inicio funcion: " + "LeerDocumentosAsync " + DateTime.Now.ToString(), modulo);

                _logger.LogInformation("************************inicio funcion LeerDocumentosAsync**************************************");
                _logger.LogInformation("Inicio funcion: " + "LeerDocumentosAsync " + DateTime.Now.ToString());


                dt_config = _cache.GetOrCreate("dt_config", cacheEntry =>
                {
                    return PivoteDataTable.Pivot(func.Get_configuraciones(OracleConnStr, esquema));
                });

                JConfigString = JsonConvert.SerializeObject(dt_config).Replace("[", "").Replace("]", "");
                dynamic do_config = JObject.Parse(JConfigString);

                #region asignacion_variables
                securityToken.username = do_config["username"];
                securityToken.password = do_config["password"];
                url_security = do_config["url_security"];
                url_reception = do_config["url_reception"];
                url_base = do_config["url_security"];
                Col_IdDocEnc = do_config["Col_IdDocEnc"];
                Col_TipoDTE = do_config["Col_TipoDTE"];
                Tbl_DocEncabezado = do_config["Tbl_DocEncabezado"];
                Col_resp = do_config["Col_resp"];
                PeriodoMinutos = do_config["PeriodoMinutos"];
                Tbl_log = do_config["Tbl_log"];
                Col_Estado = do_config["Col_Estado"];
                Col_sello = do_config["Col_sello"];
                Col_UidDocEnc = do_config["Col_UidDocEnc"];
                Col_TotalVentaGravada = do_config["Col_TotalVentaGravada"];
                Col_Reintentos = do_config["Col_Reintentos"];
                Num_Reintentos = Convert.ToInt16(do_config["Num_Reintentos"]);
                Tbl_sefclientconf = do_config["Tbl_sefclientconf"];
                Coldtetype_config = do_config["Coldtetype_config"];
                SqlApendices = do_config["SqlApendices"];
                estado_servicio_reception = do_config["estado_servicio_reception"];
                SqlUpdateContingencia = do_config["SqlUpdateContingencia"];
                #endregion

                Sqlselectdoc = func.select_querytouse(OracleConnStr, Tbl_sefclientconf, esquema, _envconfig.GetConnectionString("QueryOnDemand"), _logger, _fileLogger, modulo);

                stopwatch_dataset.Start();
                dtdoc = func.Getdt(OracleConnStr, Sqlselectdoc);
                stopwatch_dataset.Stop();
                var tiempoDeRespuesta_dataset = stopwatch_dataset.ElapsedMilliseconds;
                _logger.LogInformation($"Tiempo en traer registros: {tiempoDeRespuesta_dataset} milisegundos.");

                //func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Proceso", "Inicio Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
                //func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Funcion", "Inicio Funcion LeerDocumentosAsync " + DateTime.Now.ToString(), "", esquema, "");
                _fileLogger.Log("Inicio Proceso: " + "Inicio Proceso a las " + DateTime.Now.ToString(), modulo);
                _logger.LogInformation("************************inicio**************************************");

                JSONST = JsonConvert.SerializeObject(securityToken);


                respservicio = func.ObtenerOCrearCache(_cache, "resp_tkservice", url_security, JSONST, _logger, _fileLogger, "LeerDocumentosAsync", modulo);

                if (respservicio.LastIndexOf("token") > -1)
                {
                    // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Token", respservicio, "", esquema, "");
                    tk = JObject.Parse(respservicio);

                    token = Convert.ToString(tk["token"]);
                    //_logger.LogInformation("token: " + token);
                    //_fileLogger.Log("token: " + token);
                }
                else
                {
                    _logger.LogInformation("Error: " + respservicio);
                    _fileLogger.Log("Error: " + respservicio, modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", respservicio, "", esquema, "");
                }


                if (dtdoc.Rows.Count > 0)
                {
                    _logger.LogInformation("Registros a procesar: " + dtdoc.Rows.Count.ToString());
                    _fileLogger.Log("Registros a procesar: " + dtdoc.Rows.Count.ToString(), modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "LeerDocumentosAsync", "Numero de Registros a Procesar: " + dtdoc.Rows.Count.ToString(), "", esquema, "");

                    for (int i = 0; i < dtdoc.Rows.Count; i++)
                    {
                        stopwatch_row.Start();
                        dr = dtdoc.Rows[i];
                        idrow = Convert.ToInt32(dr[Col_IdDocEnc].ToString());
                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "P", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                        func.update_FGENERACION(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "P", idrow.ToString(), esquema, "");
                        _fileLogger.Log("Fila a Procesar: " + dr.ToString(), modulo);
                        _logger.LogInformation("IdRegistro: " + idrow);
                        _fileLogger.Log("IdRegistro: " + idrow, modulo);
                        try
                        {
                            do_json = func.GeneraNodoRootJsonDinamico(OracleConnStr, esquema, dr[Col_TipoDTE].ToString(), dr, do_config, Col_IdDocEnc, _logger, Col_TotalVentaGravada);
                        }
                        catch (Exception ex)
                        {
                            if (ex.ToString().Contains("Newtonsoft.Json.JsonReaderException"))
                            {
                                int posini = ex.ToString().IndexOf("Path") + 5;
                                _fileLogger.Log("posini: " + posini.ToString(), modulo);
                                _fileLogger.Log("posini: " + posini.ToString(), modulo);
                                string tmp = ex.ToString().Substring(posini);
                                _fileLogger.Log("tmp: " + tmp, modulo);
                                int posfinal = tmp.ToString().IndexOf(",");
                                _fileLogger.Log("posfinal: " + posfinal.ToString(), modulo);

                                string campo = tmp.Substring(0, posfinal);
                                _logger.LogInformation("campo: " + campo);
                                _fileLogger.Log("campo: " + campo, modulo);
                                _fileLogger.Log("Error GeneraNodoRootJsonDinamico: No se pudo formar bien el JSON, problemas de datos en el campo: " + campo, modulo);
                                _logger.LogInformation("Error GeneraNodoRootJsonDinamico: No se pudo formar bien el JSON, problemas de datos en el campo: " + campo);

                            }
                            else
                            {
                                _fileLogger.Log("Error GeneraNodoRootJsonDinamico: " + ex.ToString(), modulo);
                                _logger.LogInformation("Error GeneraNodoRootJsonDinamico: " + ex.ToString());
                            }
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                            break;
                        }

                        string l = JsonConvert.SerializeObject(do_json);

                        _fileLogger.Log("JSON ORIGINAL: " + l, modulo);

                        jsonstr = func.fix_json(l, _logger, dr[Col_TipoDTE].ToString());

                        _fileLogger.Log("JSON CON FIX: " + jsonstr, modulo);
                        /**************************************Busca pendices en la tabla*******************************/
                        _fileLogger.Log("QUERY APENDICES: " + SqlApendices + idrow.ToString(), modulo);
                        _logger.LogInformation("QUERY APENDICES: " + SqlApendices + idrow.ToString());
                        dt_apendices = func.Getdt(OracleConnStr, SqlApendices + idrow.ToString());
                        _fileLogger.Log("APENDICES ENCONTRADOS: " + dt_apendices.Rows.Count.ToString(), modulo);
                        _logger.LogInformation("APENDICES ENCONTRADOS: " + dt_apendices.Rows.Count.ToString());
                        if (dt_apendices.Rows.Count > 0)
                        {
                            jsonstr_apendices = func.apendices_json(jsonstr, _logger, dt_apendices);
                            jsonstr = jsonstr_apendices;
                            _fileLogger.Log("JSON CON APENDICES: " + jsonstr, modulo);
                        }
                        else
                        {
                            _fileLogger.Log("JSON NO TIENE APENDICES: " + jsonstr, modulo);
                        }
                        /*************************************************************************************************/

                        do_json = JObject.Parse(jsonstr);
                        ambiente = (string)do_json.ambiente;
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Json Envio OnDemand", jsonstr, idrow.ToString(), esquema, "");
                        func.update_datos_enc_od(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_IdDocEnc, jsonstr);


                        string CONTINGENCIAMH = func.prop_valor(OracleConnStr, func.select_config_valor("CONTINGENCIAMH", "CONFIGURACION", "", esquema));

                        _logger.LogInformation("CONTINGENCIABH: " + CONTINGENCIAMH);

                        _logger.LogInformation("Json a Procesar: " + jsonstr);

                        estado = func.ValidaSelloDocRelacionado(OracleConnStr, esquema, Col_sello, Tbl_DocEncabezado, Col_UidDocEnc, jsonstr);

                        _logger.LogInformation("Estado: " + estado);

                        if (estado == "No procesar")
                        {
                            nullresponse.ambiente = ambiente;
                            nullresponse.versionApp = 2;
                            nullresponse.fhProcesamiento = DateTime.Now.ToString();
                            nullresponse.numeroControl = null;
                            nullresponse.selloRecibido = null;
                            nullresponse.codigoGeneracion = null;
                            nullresponse.codigoMsg = "900";
                            nullresponse.descripcionMsg = "Documento Relacionado no Firmado";
                            nullresponse.digital = null;
                            nullresponse.observaciones = null;
                            nullresponse.estado = null;
                            var options = new JsonSerializerOptions { WriteIndented = true };
                            string jsonresponsenullString = System.Text.Json.JsonSerializer.Serialize(nullresponse, options);

                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Procesos", jsonresponsenullString, idrow.ToString(), esquema, "");
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                            func.update_mh_resp(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_resp, jsonresponsenullString, Col_IdDocEnc);
                            _logger.LogInformation("Documento Relacionado no Firmado " + jsonresponsenullString);
                            _fileLogger.Log("Documento Relacionado no Firmado" + jsonresponsenullString, modulo);
                            continue;
                        }


                        if (func.validaJson(jsonstr) == 1) //json valido
                        {
                            estado_servicio = func.verifica_servicio_reception(url_reception);
                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado Servicio", estado_servicio, idrow.ToString(), esquema, "");
                            _logger.LogInformation("El estado del servicio: " + url_reception + " es: " + estado_servicio + " se compara con el estado configurado en la tabla: " + estado_servicio_reception);
                            _fileLogger.Log("El estado del servicio: " + url_reception + " es: " + estado_servicio + " se compara con el estado configurado en la tabla: " + estado_servicio_reception, modulo);
                            if (estado_servicio == estado_servicio_reception)
                            {
                                var resp = (dynamic)null;
                                dynamic data = JObject.Parse(jsonstr);

                                try
                                {
                                    if (CONTINGENCIAMH == "0")
                                    {

                                        resp = func.SendtoReception(data, token, url_reception, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                        MHresponse = JsonConvert.SerializeObject(resp);
                                        if (string.IsNullOrEmpty((string)resp.selloRecibido))
                                        {
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.estado, idrow.ToString(), esquema, "");
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.descripcionMsg, idrow.ToString(), esquema, "");
                                            func.insert_Arraylogtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string[])resp.observaciones, idrow.ToString(), esquema, "");
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "E", idrow.ToString(), esquema, "");
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                            _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                            _logger.LogInformation("Respuesta MH: " + MHresponse);
                                            _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                            _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                            _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);


                                            if (string.IsNullOrEmpty((string)resp.estado))
                                            {
                                                func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                //valida errores si el estado es nulo
                                                switch ((string)resp.codigoMsg)
                                                {
                                                    case "990": //Sucedio un error al iniciar sesion en el Ministerio de Hacienda
                                                        _logger.LogInformation("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg);
                                                        _fileLogger.Log("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg, modulo);
                                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", ((string)resp.codigoMsg + " - " + (string)resp.descripcionMsg), idrow.ToString(), esquema, "");
                                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "I", idrow.ToString(), esquema, "");
                                                        break;

                                                }
                                            }
                                            else
                                            {
                                                if ((string)resp.estado == "RECHAZADO")
                                                {
                                                    if (((string)resp.descripcionMsg).Contains("DISPONIBLE PARA CONTINGENCIA") == true)
                                                    {
                                                        //entra en contingencia del MH y se cambia el estado al registro
                                                        func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "1");
                                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                        _logger.LogInformation("Mensaje : Documento en Contingencia entre PBS Y MH --> I");
                                                        _fileLogger.Log("Mensaje : Documento en Contingencia entre PBS Y MH --> I", modulo);
                                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia entre PBS --> MH", idrow.ToString(), esquema, "");
                                                    }
                                                    else
                                                    {
                                                        func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                        //el documento fue rechazado y no hay que hacer nada
                                                    }
                                                }
                                                else
                                                {
                                                    //contingencia

                                                    if ((string)resp.estado == "PENDIENTE" && ((string)resp.descripcionMsg).Contains("CONTINGENCIA") == true)
                                                    {
                                                        //entra en contingencia del MH y se cambia el estado al registro
                                                        func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "1");
                                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CH", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                        _logger.LogInformation("Mensaje : Documento en Contingencia entre PBS --> MH");
                                                        _fileLogger.Log("Mensaje : Documento en Contingencia entre PBS --> MH", modulo);
                                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia entre PBS --> MH", idrow.ToString(), esquema, "");
                                                    }
                                                    else
                                                    {
                                                        func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");

                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "F", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            func.update_sellomh(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "SelloMH", "El registro con id: " + idrow.ToString() + " Tiene el Sello: " + (string)resp.selloRecibido, idrow.ToString(), esquema, "");
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "F", idrow.ToString(), esquema, "");
                                            _logger.LogInformation("SelloMH: " + (string)resp.selloRecibido);
                                            _logger.LogInformation("Respuesta MH: " + MHresponse);
                                            _fileLogger.Log("SelloMH: " + (string)resp.selloRecibido, modulo);
                                            _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                            func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                            //firmado
                                        }
                                        func.update_mh_resp(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_resp, MHresponse, Col_IdDocEnc);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Respuesta MH", MHresponse, idrow.ToString(), esquema, "");
                                    }
                                    else //CONTINGENCIAMH=1
                                    {
                                        try
                                        {
                                            dynamic data1 = JObject.Parse(jsonstr);

                                            JArray documentos = (JArray)data1["cuerpoDocumento"];
                                            foreach (JToken documento in documentos)
                                            {
                                                num_doc_relacionado = (string)documento["numeroDocumento"];
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            num_doc_relacionado = string.Empty;
                                        }

                                        if (string.IsNullOrEmpty(num_doc_relacionado))
                                        {
                                            if (dr[Col_TipoDTE].ToString() == "01" || dr[Col_TipoDTE].ToString() == "03" || dr[Col_TipoDTE].ToString() == "04" || dr[Col_TipoDTE].ToString() == "05" || dr[Col_TipoDTE].ToString() == "06" || dr[Col_TipoDTE].ToString() == "11")
                                            {
                                                resp = func.SendtoReception(data, token, url_reception, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                                MHresponse = JsonConvert.SerializeObject(resp);
                                                if (string.IsNullOrEmpty((string)resp.selloRecibido))
                                                {
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.estado, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.descripcionMsg, idrow.ToString(), esquema, "");
                                                    func.insert_Arraylogtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string[])resp.observaciones, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "E", idrow.ToString(), esquema, "");
                                                    func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                    _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                                    _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                                    _logger.LogInformation("Respuesta MH: " + MHresponse);
                                                    _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                                    _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                                    _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);

                                                    if (string.IsNullOrEmpty((string)resp.estado))
                                                    {
                                                        //func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                        //valida errores si el estado es nulo
                                                        switch ((string)resp.codigoMsg)
                                                        {
                                                            case "990": //Sucedio un error al iniciar sesion en el Ministerio de Hacienda
                                                                _logger.LogInformation("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg);
                                                                _fileLogger.Log("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg, modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", ((string)resp.codigoMsg + " - " + (string)resp.descripcionMsg), idrow.ToString(), esquema, "");
                                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "I", idrow.ToString(), esquema, "");
                                                                break;

                                                        }
                                                    }
                                                    else
                                                    {
                                                        if ((string)resp.estado == "RECHAZADO")
                                                        {
                                                            func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                            //el documento fue rechazado y no hay que hacer nada
                                                        }
                                                        else
                                                        {
                                                            //contingencia

                                                            if ((string)resp.estado == "PENDIENTE" && ((string)resp.descripcionMsg).Contains("CONTINGENCIA") == true)
                                                            {
                                                                //entra en contingencia del MH y se cambia el estado al registro
                                                                //func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "1");
                                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CH", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                                _logger.LogInformation("Mensaje : Documento en Contingencia en Tablas Intermedias PBS --> MH");
                                                                _fileLogger.Log("Mensaje : Documento en Contingencia en Tablas Intermedias PBS --> MH", modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia en PBS --> MH", idrow.ToString(), esquema, "");
                                                            }
                                                            else
                                                            {
                                                                //func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");

                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "F", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                    func.update_sellomh(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "SelloMH", "El registro con id: " + idrow.ToString() + " Tiene el Sello: " + (string)resp.selloRecibido, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "F", idrow.ToString(), esquema, "");
                                                    _logger.LogInformation("SelloMH: " + (string)resp.selloRecibido);
                                                    _logger.LogInformation("Respuesta MH: " + MHresponse);
                                                    _fileLogger.Log("SelloMH: " + (string)resp.selloRecibido, modulo);
                                                    _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                                    func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                    //firmado
                                                }
                                                func.update_mh_resp(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_resp, MHresponse, Col_IdDocEnc);
                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Respuesta MH", MHresponse, idrow.ToString(), esquema, "");
                                            }
                                            else
                                            {
                                                //es un documento no permitido
                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            }
                                        }
                                        else
                                        {
                                            //documento relacionado<>null
                                            if (dr[Col_TipoDTE].ToString() == "01" || dr[Col_TipoDTE].ToString() == "03" || dr[Col_TipoDTE].ToString() == "07" || dr[Col_TipoDTE].ToString() == "08" || dr[Col_TipoDTE].ToString() == "09" || dr[Col_TipoDTE].ToString() == "14")
                                            {
                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            }
                                            else
                                            {
                                                resp = func.SendtoReception(data, token, url_reception, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                                MHresponse = JsonConvert.SerializeObject(resp);
                                                if (string.IsNullOrEmpty((string)resp.selloRecibido))
                                                {
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.estado, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string)resp.descripcionMsg, idrow.ToString(), esquema, "");
                                                    func.insert_Arraylogtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", (string[])resp.observaciones, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "E", idrow.ToString(), esquema, "");
                                                    func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                    _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                                    _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                                    _logger.LogInformation("Respuesta MH: " + MHresponse);
                                                    _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                                    _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                                    _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);

                                                    if (string.IsNullOrEmpty((string)resp.estado))
                                                    {
                                                        func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                        //valida errores si el estado es nulo
                                                        switch ((string)resp.codigoMsg)
                                                        {
                                                            case "990": //Sucedio un error al iniciar sesion en el Ministerio de Hacienda
                                                                _logger.LogInformation("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg);
                                                                _fileLogger.Log("Mensaje Error: " + (string)resp.codigoMsg + " - " + (string)resp.descripcionMsg, modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoReception", ((string)resp.codigoMsg + " - " + (string)resp.descripcionMsg), idrow.ToString(), esquema, "");
                                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "I", idrow.ToString(), esquema, "");
                                                                break;

                                                        }
                                                    }
                                                    else
                                                    {
                                                        if ((string)resp.estado == "RECHAZADO")
                                                        {
                                                            func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                            //el documento fue rechazado y no hay que hacer nada
                                                        }
                                                        else
                                                        {
                                                            //contingencia

                                                            if ((string)resp.estado == "PENDIENTE" && ((string)resp.descripcionMsg).Contains("CONTINGENCIA") == true)
                                                            {
                                                                //entra en contingencia del MH y se cambia el estado al registro
                                                                //func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "1");
                                                                func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CH", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                                _logger.LogInformation("Mensaje : Documento en Contingencia en Tablas Intermedias PBS --> MH");
                                                                _fileLogger.Log("Mensaje : Documento en Contingencia en Tablas Intermedias PBS --> MH", modulo);
                                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia en PBS --> MH", idrow.ToString(), esquema, "");
                                                            }
                                                            else
                                                            {
                                                                func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");

                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "F", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                                    func.update_sellomh(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "SelloMH", "El registro con id: " + idrow.ToString() + " Tiene el Sello: " + (string)resp.selloRecibido, idrow.ToString(), esquema, "");
                                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "F", idrow.ToString(), esquema, "");
                                                    _logger.LogInformation("SelloMH: " + (string)resp.selloRecibido);
                                                    _logger.LogInformation("Respuesta MH: " + MHresponse);
                                                    _fileLogger.Log("SelloMH: " + (string)resp.selloRecibido, modulo);
                                                    _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                                    func.update_valor_contingency(OracleConnStr, Tbl_sefclientconf, esquema, "0");
                                                    //firmado
                                                }
                                                func.update_mh_resp(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_resp, MHresponse, Col_IdDocEnc);
                                                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Respuesta MH", MHresponse, idrow.ToString(), esquema, "");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (string.IsNullOrEmpty(resp))
                                    {

                                        _logger.LogInformation("Error: Respuesta Vacia el MH esta Saturado");
                                        _fileLogger.Log("Error: Respuesta Vacia el MH esta Saturado", modulo);
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "EC", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                    }
                                    else
                                    {
                                        _logger.LogInformation("Error: " + ex.ToString());
                                        _fileLogger.Log("Error: " + ex.ToString(), modulo);
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString(), idrow.ToString(), esquema, "");
                                    }
                                }
                            }
                            else
                            {
                                //si no hay servicio de PBS hace contingencia
                                //reintentos
                                if (string.IsNullOrEmpty(dr["REINTENTOS"].ToString()))
                                {
                                    reintentos = 1;
                                }
                                else
                                {
                                    reintentos = Convert.ToInt32(dr["REINTENTOS"].ToString()) + 1;
                                }

                                if (reintentos <= Num_Reintentos)
                                {
                                    func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "I", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                    func.update_reintentos(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Reintentos, reintentos.ToString(), Col_IdDocEnc);
                                    _logger.LogInformation("Mensaje : Reintento de envio # " + reintentos.ToString());
                                    _fileLogger.Log("Mensaje : Reintento de envio # " + reintentos.ToString(), modulo);
                                }
                                else
                                {
                                    try
                                    {
                                        dynamic data1 = JObject.Parse(jsonstr);

                                        JArray documentos = (JArray)data1["cuerpoDocumento"];
                                        foreach (JToken documento in documentos)
                                        {
                                            num_doc_relacionado = (string)documento["numeroDocumento"];
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        num_doc_relacionado = string.Empty;
                                    }

                                    _logger.LogInformation("num_doc_relacionado: " + num_doc_relacionado);
                                    _fileLogger.Log("num_doc_relacionado: " + num_doc_relacionado, modulo);

                                    if (string.IsNullOrEmpty(num_doc_relacionado))
                                    {
                                        if (dr[Col_TipoDTE].ToString() == "01" || dr[Col_TipoDTE].ToString() == "03" || dr[Col_TipoDTE].ToString() == "04" || dr[Col_TipoDTE].ToString() == "05" || dr[Col_TipoDTE].ToString() == "06" || dr[Col_TipoDTE].ToString() == "11")
                                        {
                                            //se actualizan los valores para contingencia y se coloca en estado C
                                            func.update_estado_valores_contingency(OracleConnStr, SqlUpdateContingencia + idrow.ToString());
                                            _logger.LogInformation("Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS");
                                            _fileLogger.Log("Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS", modulo);
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS", idrow.ToString(), esquema, "");
                                        }
                                        else
                                        {
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CI", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            _logger.LogInformation("Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS");
                                            _fileLogger.Log("Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS", modulo);
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS", idrow.ToString(), esquema, "");
                                        }
                                    }
                                    else
                                    {
                                        //num_doc_relacionado <> null
                                        if (dr[Col_TipoDTE].ToString() == "01" || dr[Col_TipoDTE].ToString() == "03" || dr[Col_TipoDTE].ToString() == "07" || dr[Col_TipoDTE].ToString() == "08" || dr[Col_TipoDTE].ToString() == "09" || dr[Col_TipoDTE].ToString() == "14")
                                        {
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CI", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                                            _logger.LogInformation("Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS");
                                            _fileLogger.Log("Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS", modulo);
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia Relacionado en Tablas Intermedias Cliente --> PBS", idrow.ToString(), esquema, "");
                                        }
                                        else
                                        {
                                            //se coloca en estado C
                                            func.update_estado_valores_contingency(OracleConnStr, SqlUpdateContingencia + idrow.ToString());
                                            _logger.LogInformation("Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS");
                                            _fileLogger.Log("Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS", modulo);
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Contingencia", "Mensaje : Documento en Contingencia en Tablas Intermedias Cliente --> PBS", idrow.ToString(), esquema, "");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            _logger.LogInformation("Error: JSON Invalido");
                            _fileLogger.Log("Error: JSON Invalido", modulo);
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAsync", modulo);
                        }
                        stopwatch_row.Stop();
                        var tiempoProcesoFila = stopwatch_row.ElapsedMilliseconds;
                        _logger.LogInformation($"Tiempo en ejecutar la fila con ROWID={idrow} fue de {tiempoProcesoFila} milisegundos.");
                        dr = null;
                    }
                    //func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Funcion", "Fin Funcion LeerDocumentosAsync " + DateTime.Now.ToString(), idrow.ToString(), esquema, "");
                }
                else
                {
                    _logger.LogInformation("No hay registros a procesar!!!!");
                    _logger.LogInformation("************************Fin Funucion**************************************");
                    _fileLogger.Log("No hay registros a procesar!!!!", modulo);
                    _fileLogger.Log("************************Fin Funucion**************************************", modulo);
                    //func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "No hay registros a procesar!!!! " + DateTime.Now.ToString(), "", esquema, "");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error: " + ex.ToString());
                if (ex.ToString().Contains("System.NullReferenceException"))
                {
                    _logger.LogInformation("Error: " + ex.ToString());
                    _fileLogger.Log("Error: " + ex.ToString(), modulo);
                }

                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString() + " " + DateTime.Now.ToString(), "", esquema, "");
            }
            finally
            {
                _logger.LogInformation("************************Fin Proceso**************************************");
                _fileLogger.Log("************************Fin Proceso**************************************", modulo);
                //  func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "Fin Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
            }

            stopwatch.Stop();
            var tiempoDeRespuesta = stopwatch.ElapsedMilliseconds;
            _logger.LogInformation($"Tiempo en ejecutar el proceso: {modulo} fue: {tiempoDeRespuesta} milisegundos.");
            await Task.Delay(TimeSpan.FromMinutes(PeriodoMinutos), stoppingToken);
        }

        public async Task LeerDocumentosContingencyAsync(CancellationToken stoppingToken)
        {
            #region declaracion_variables
            SEFClientFunctions func = new SEFClientFunctions();
            string jsonstr = string.Empty;
            dynamic do_json = new JObject();
            dynamic do_json_nodos = new JObject();
            dynamic tk = new JObject();
            string token = string.Empty;
            string respservicio = string.Empty;
            DataTable dtdoc = new DataTable();
            DataTable dt_config = new DataTable();
            DataTable dt_jcontingency_event = new DataTable();
            DataTable dt_contingency_val = new DataTable();
            DataTable dt_DetalleDTE = new DataTable();
            DataView dv = new DataView();
            DataRow dr;
            int idrow = 0;
            string estado_servicio = string.Empty;
            Config.SecurityToken securityToken = new Config.SecurityToken();
            string JSONST = string.Empty;
            string JConfigString = string.Empty;
            string url_reception = string.Empty;
            string url_security = string.Empty;
            string url_base = string.Empty;
            string url_Contingencia = string.Empty;
            string Col_IdDocEnc = string.Empty;
            string Sqlselectdoc = string.Empty;
            string SqlQueryDatosContingencia = string.Empty;
            string SqlDetalleDTE = string.Empty;
            string Company = string.Empty;
            string Col_TipoDTE = string.Empty;
            string Coldtetype_config = string.Empty;
            string Tbl_DocEncabezado = string.Empty;
            string Col_resp = string.Empty;
            string Tbl_log = string.Empty;
            string Col_Estado = string.Empty;
            string Col_sello = string.Empty;
            string Col_UidDocEnc = string.Empty;
            string Col_Reintentos = string.Empty;
            string Contingency_json = string.Empty;
            string Tbl_sefclientconf = string.Empty;
            int PeriodoMinutos = 0;
            string estado_servicio_reception = string.Empty;
            string OracleConnStr = string.Empty;
            string esquema = string.Empty;
            string Col_TotalVentaGravada = string.Empty;
            string querytouse = string.Empty;
            string QueryDatosContingencia = string.Empty;
            string modulo = string.Empty;

            #endregion

            modulo = "Contingencia";

            #region conexiondb

            if (_envconfig.GetConnectionString("ASPNETCORE_ENVIRONMENT") == "Development")
            {

                OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 192.168.139.4)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = XE)));User ID=BHDTE;Password=Sefclient123;";
                //OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 10.10.3.152)(PORT = 6120)))(CONNECT_DATA =(SERVICE_NAME = DESA02)));User ID=OJGARCIA_HIGHTECH;Password=Banco2023;";
                esquema = "BHDTE";
                _fileLogger.Log("OracleConnStr: " + OracleConnStr, modulo);
                _logger.LogInformation("OracleConnStr: " + OracleConnStr);

            }
            else
            {
                OracleConnStr = func.OracleConnStr(_envconfig.GetConnectionString("DbHost"), _envconfig.GetConnectionString("DbPort"), _envconfig.GetConnectionString("DbSERVICENAME"), _envconfig.GetConnectionString("DbUserId"), _envconfig.GetConnectionString("DbPassword"));
                esquema = _envconfig.GetConnectionString("Esquema");
            }


            //_logger.LogInformation("OracleConnStr: " + OracleConnStr);
            _logger.LogInformation("ConexionDB: " + func.TestConexionDB(OracleConnStr));
            _fileLogger.Log("ConexionDB: " + func.TestConexionDB(OracleConnStr), modulo);

            #endregion

            try
            {

                _logger.LogInformation("*****************************************************************************");
                _logger.LogInformation("Inicio Funcion LeerDocumentosContingencyAsync" + DateTime.Now.ToString());
                _logger.LogInformation("*****************************************************************************");
                _fileLogger.Log("*****************************************************************************", modulo);
                _fileLogger.Log("Inicio Funcion LeerDocumentosContingencyAsync" + DateTime.Now.ToString(), modulo);
                _fileLogger.Log("*****************************************************************************", modulo);
                _logger.LogInformation("QueryContingency: " + _envconfig.GetConnectionString("QueryContingency"));
                _fileLogger.Log("QueryContingency: " + _envconfig.GetConnectionString("QueryContingency"), modulo);

                dt_config = _cache.GetOrCreate("dt_config", cacheEntry => {
                    return PivoteDataTable.Pivot(func.Get_configuraciones(OracleConnStr, esquema, _logger));
                });

                JConfigString = JsonConvert.SerializeObject(dt_config).Replace("[", "").Replace("]", "");
                dynamic do_config = JObject.Parse(JConfigString);

                #region asignacion_variables

                securityToken.username = do_config["username"];
                securityToken.password = do_config["password"];
                url_security = do_config["url_security"];
                url_reception = do_config["url_reception"];
                url_base = do_config["url_security"];
                url_Contingencia = do_config["url_Contingencia"];
                Col_IdDocEnc = do_config["Col_IdDocEnc"];
                Sqlselectdoc = do_config["SqlselectContingencydoc"];
                SqlDetalleDTE = do_config["SqlDetalleDTE"];
                Col_TipoDTE = do_config["Col_TipoDTE"];
                Tbl_DocEncabezado = do_config["Tbl_DocEncabezado"];
                Col_resp = do_config["Col_resp"];
                PeriodoMinutos = do_config["PeriodoMinutosContingencia"];
                Tbl_log = do_config["Tbl_log"];
                Col_Estado = do_config["Col_Estado"];
                Col_sello = do_config["Col_sello"];
                Col_UidDocEnc = do_config["Col_UidDocEnc"];
                Col_Reintentos = do_config["Col_Reintentos"];
                Col_TotalVentaGravada = do_config["Col_TotalVentaGravada"];
                Contingency_json = do_config["Contingency_json"];
                Tbl_sefclientconf = do_config["Tbl_sefclientconf"];
                Coldtetype_config = do_config["Coldtetype_config"];
                Company = do_config["Company"];
                estado_servicio_reception = do_config["estado_servicio_reception"];

                SqlQueryDatosContingencia = func.select_querytouse(OracleConnStr, Tbl_sefclientconf, esquema, _envconfig.GetConnectionString("QueryDatosContingencia"), _logger, _fileLogger, modulo);

                dtdoc = func.Getdt(OracleConnStr, Sqlselectdoc);
                dt_contingency_val = func.Getdt(OracleConnStr, SqlQueryDatosContingencia);
                dt_jcontingency_event = func.Getdt(OracleConnStr, func.jcontingency_event(Tbl_sefclientconf, esquema, Coldtetype_config, Contingency_json));

                JArray documentos_contingencia = new JArray();
                JArray docs_event_contingencia = new JArray();

                #endregion

                // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Proceso", "Inicio Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
                // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Funcion", "Inicio Funcion LeerDocumentosContingencyAsync " + DateTime.Now.ToString(), "", esquema, "");

                JSONST = JsonConvert.SerializeObject(securityToken);

                respservicio = func.ObtenerOCrearCache(_cache, "resp_tkservice", url_security, JSONST, _logger, _fileLogger, "LeerDocumentosContingencyAsync", modulo);

                if (respservicio.LastIndexOf("token") > 0)
                {
                    // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Token", respservicio, "", esquema, "");
                    tk = JObject.Parse(respservicio);
                    token = Convert.ToString(tk["token"]);
                }
                else
                {
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", respservicio, "", esquema, "");
                }


                if (dtdoc.Rows.Count > 0)
                {
                    JArray documentos_detalle = new JArray();
                    JArray docs_evento = new JArray();

                    _fileLogger.Log("Registros a procesar en Contingencia: " + dtdoc.Rows.Count.ToString(), modulo);
                    _logger.LogInformation("Registros a procesar en Contingencia: " + dtdoc.Rows.Count.ToString());
                    _fileLogger.Log("ConexionDB: " + func.TestConexionDB(OracleConnStr), modulo);

                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "LeerDocumentosContingencyAsync", "Numero de Registros a Procesar: " + dtdoc.Rows.Count.ToString(), "", esquema, "");
                    for (int i = 0; i < dtdoc.Rows.Count; i++)
                    {
                        dr = dtdoc.Rows[i];
                        idrow = Convert.ToInt32(dr[Col_IdDocEnc].ToString());
                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "P", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosContingencyAsync", modulo);
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "P", idrow.ToString(), esquema, "");

                        do_json = func.GeneraNodoRootJsonDinamico(OracleConnStr, esquema, dr[Col_TipoDTE].ToString(), dr, do_config, Col_IdDocEnc, _logger, Col_TotalVentaGravada);

                        string l = JsonConvert.SerializeObject(do_json);

                        _fileLogger.Log("JSON ORIGINAL CONTINGENCIA: " + l, modulo);
                        _logger.LogInformation("JSON ORIGINAL CONTINGENCIA: " + l);

                        jsonstr = func.fix_json(l, _logger, dr[Col_TipoDTE].ToString());

                        _fileLogger.Log("JSON CON FIX CONTINGENCIA: " + jsonstr, modulo);
                        _logger.LogInformation("JSON CON FIX CONTINGENCIA: " + jsonstr);

                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Json Envio Contingencia", jsonstr, idrow.ToString(), esquema, "");


                        if (func.validaJson(jsonstr) == 1) //json valido
                        {
                            JObject data = JObject.Parse(jsonstr);
                            documentos_detalle.Add(data);
                            dynamic docs = new JObject();

                            docs.Add("noItem", i + 1);
                            docs.Add("codigoGeneracion", (string)data.SelectToken("identificacion.codigoGeneracion"));
                            docs.Add("tipoDoc", (string)data.SelectToken("identificacion.tipoDte"));
                            docs_evento.Add(docs);
                        }


                        dr = null;
                    }
                    documentos_contingencia = documentos_detalle;
                    docs_event_contingencia = docs_evento;

                    _fileLogger.Log("documentos_contingencia: " + JsonConvert.SerializeObject(documentos_contingencia), modulo);
                    _logger.LogInformation("docs_event_contingencia: " + JsonConvert.SerializeObject(documentos_contingencia));

                    _fileLogger.Log("documentos_contingencia: " + JsonConvert.SerializeObject(docs_event_contingencia), modulo);
                    _logger.LogInformation("docs_event_contingencia: " + JsonConvert.SerializeObject(docs_event_contingencia));


                    /************************************************VALIDA SERVICIO********************************************/
                    estado_servicio = func.verifica_servicio_reception(url_reception);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado Servicio", estado_servicio, idrow.ToString(), esquema, "");
                    if (estado_servicio == estado_servicio_reception)
                    {
                        _fileLogger.Log("estado_servicio_reception: " + estado_servicio, modulo);
                        _logger.LogInformation("estado_servicio_reception: " + estado_servicio);

                        _fileLogger.Log("estado_servicio_reception_entabla: " + estado_servicio_reception, modulo);
                        _logger.LogInformation("estado_servicio_reception_entabla: " + estado_servicio_reception);

                        string jcontingency_event = string.Empty;

                        try
                        {
                            jcontingency_event = func.json_contingency_event(dt_jcontingency_event, dt_contingency_val, docs_event_contingencia);
                        }
                        catch (Exception ex)
                        {
                            _fileLogger.Log("Exepcion en evento: " + ex.ToString(), modulo);
                            _logger.LogInformation("Exepcion en evento: " + ex.ToString());
                        }


                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Funcion", "Fin Funcion LeerDocumentosAsync " + DateTime.Now.ToString(), idrow.ToString(), esquema, "");
                        dynamic data = JObject.Parse(jcontingency_event);
                        try
                        {
                            _logger.LogInformation("JSON EVENTO CONTINGENCIA ENVIADO: " + jcontingency_event);
                            _fileLogger.Log("JSON EVENTO CONTINGENCIA ENVIADO: " + jcontingency_event, modulo);

                            var resp_contingency = func.SendtocContingencyEvent(data, token, url_Contingencia, do_config, OracleConnStr, esquema, _logger);
                            string MHresponse = JsonConvert.SerializeObject(resp_contingency);
                            _logger.LogInformation("Respuesta MH: " + MHresponse);
                            _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                            if (string.IsNullOrEmpty((string)resp_contingency.selloRecibido))
                            {
                                //SI EL EVENTO FUE RECHAZADO
                                for (int y = 0; y < documentos_contingencia.Count; y++)
                                {
                                    string codigogeneracion = (string)documentos_contingencia[y].SelectToken("identificacion.codigoGeneracion");
                                    func.update_estado_by_uid(OracleConnStr, Tbl_DocEncabezado, codigogeneracion, esquema, Col_Estado, "C", Col_UidDocEnc);
                                    _logger.LogInformation("El Codigo Generacion: " + codigogeneracion + " paso a estado: C");
                                    _fileLogger.Log("El Codigo Generacion: " + codigogeneracion + " paso a estado: C", modulo);
                                }
                                _logger.LogInformation("Error: el documento ha sido " + (string)resp_contingency.estado);
                                _fileLogger.Log("Error: el documento ha sido " + (string)resp_contingency.estado, modulo);
                            }
                            else
                            {
                                //func.insert_logtbl(SqlConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "F", idrow.ToString(), esquema, "");
                                //func.update_estado(SqlConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "F", Col_IdDocEnc);
                                //func.update_sellomh(SqlConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);

                                _logger.LogInformation("Sello Evento Contingencia MH: " + (string)resp_contingency.selloRecibido);
                                _fileLogger.Log("Sello Evento Contingencia MH: " + (string)resp_contingency.selloRecibido, modulo);
                                //firmado
                                for (int y = 0; y < documentos_contingencia.Count; y++)
                                {
                                    string codigogeneracion = (string)documentos_contingencia[y].SelectToken("identificacion.codigoGeneracion");
                                    var resp1 = func.SendtoReception(documentos_contingencia[y], token, url_reception, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                    string MHresponse1 = JsonConvert.SerializeObject(resp1);

                                    _logger.LogInformation("Respuesta MH en contingencia: " + MHresponse1);
                                    _fileLogger.Log("Respuesta MH en contingencia: " + MHresponse1, modulo);

                                    if (string.IsNullOrEmpty((string)resp1.selloRecibido))
                                    {
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con codigogeneracion: " + codigogeneracion + " paso a estado: " + "EC", codigogeneracion, esquema, "");
                                        func.update_estado_by_uid(OracleConnStr, Tbl_DocEncabezado, codigogeneracion, esquema, Col_Estado, "EC", Col_UidDocEnc);
                                        _logger.LogInformation("El Codigo Generacion: " + codigogeneracion + " paso a estado: EC");
                                        _fileLogger.Log("El Codigo Generacion: " + codigogeneracion + " paso a estado: EC", modulo);

                                        _logger.LogInformation("Respuesta MH: " + MHresponse1);
                                        _fileLogger.Log("Respuesta MH: " + MHresponse1, modulo);
                                    }
                                    else
                                    {
                                        func.update_sellomh_by_uid(OracleConnStr, Tbl_DocEncabezado, codigogeneracion, esquema, Col_sello, (string)resp1.selloRecibido, Col_UidDocEnc);
                                        func.update_estado_by_uid(OracleConnStr, Tbl_DocEncabezado, codigogeneracion, esquema, Col_Estado, "FC", Col_UidDocEnc);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con codigogeneracion: " + codigogeneracion + " paso a estado: " + "FC", codigogeneracion, esquema, "");
                                        _logger.LogInformation("Sello MH: " + (string)resp1.selloRecibido + " a codigo generacion: " + codigogeneracion);
                                        _logger.LogInformation("El Codigo Generacion: " + codigogeneracion + " paso a estado: FC");
                                        _fileLogger.Log("El Codigo Generacion: " + codigogeneracion + " paso a estado: FC", modulo);

                                        _logger.LogInformation("Respuesta MH: " + MHresponse1);
                                        _fileLogger.Log("Respuesta MH: " + MHresponse1, modulo);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInformation("Error: Ocurrio un error al tratar de obtener la respuesta del MH");
                            _fileLogger.Log("Error: Ocurrio un error al tratar de obtener la respuesta del MH", modulo);
                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString(), idrow.ToString(), esquema, "");
                        }
                    }
                    else
                    {
                        for (int y = 0; y < documentos_contingencia.Count; y++)
                        {
                            string codigogeneracion = (string)documentos_contingencia[y].SelectToken("identificacion.codigoGeneracion");
                            func.update_estado_by_uid(OracleConnStr, Tbl_DocEncabezado, codigogeneracion, esquema, Col_Estado, "C", Col_UidDocEnc);
                            _logger.LogInformation("El Codigo Generacion: " + codigogeneracion + " paso a estado: C");
                            _fileLogger.Log("El Codigo Generacion: " + codigogeneracion + " paso a estado: C", modulo);
                        }
                        _logger.LogInformation("Error: El servicio de PBS no esta Disponible !!!!!");
                        _fileLogger.Log("Error: El servicio de PBS no esta Disponible !!!!!", modulo);
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", "El servicio de PBS no esta Disponible !!!!!", idrow.ToString(), esquema, "");
                    }
                }
                else
                {
                    _logger.LogInformation("No hay registros a procesar en contingencia!!!!");
                    _fileLogger.Log("No hay registros a procesar en contingencia!!!!", modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "No hay registros a procesar en contingencia!!!! " + DateTime.Now.ToString(), "", esquema, "");
                }

            }
            catch (Exception ex)
            {
                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString() + " " + DateTime.Now.ToString(), "", esquema, "");
            }
            finally
            {
                //func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "Fin Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
            }

            _logger.LogInformation("Proceso Finalizado!!!!!!!");
            _fileLogger.Log("Proceso Finalizado!!!!!!!", modulo);
            await Task.Delay(TimeSpan.FromMinutes(PeriodoMinutos), stoppingToken);

        }

        public async Task LeerDocumentosAnuladosAsync(CancellationToken stoppingToken)
        {
            #region variables
            SEFClientFunctions func = new SEFClientFunctions();
            string jsonstr = string.Empty;
            dynamic do_json = new JObject();
            dynamic do_json_nodos = new JObject();
            dynamic tk = new JObject();
            string token = string.Empty;
            string respservicio = string.Empty;
            DataTable dtdoc = new DataTable();
            DataTable dt_config = new DataTable();
            DataTable dt_invalidation = new DataTable();

            DataView dv = new DataView();
            DataRow dr;
            int idrow = 0;
            string estado_servicio = string.Empty;
            string estado_servicio_invalidate = string.Empty;

            string MHresponse = string.Empty;
            Config.SecurityToken securityToken = new Config.SecurityToken();
            string JSONST = string.Empty;
            string JConfigString = string.Empty;
            string url_reception = string.Empty;
            string url_security = string.Empty;
            string url_base = string.Empty;
            string url_cancelation = string.Empty;
            string Col_IdDocEnc = string.Empty;
            string Sqlselectdoc = string.Empty;
            string Company = string.Empty;
            string Col_TipoDTE = string.Empty;
            string Coldtetype_config = string.Empty;
            string Tbl_DocEncabezado = string.Empty;
            string Col_resp = string.Empty;
            string Tbl_log = string.Empty;
            string Col_Estado = string.Empty;
            string Col_sello = string.Empty;
            string Col_UidDocEnc = string.Empty;
            string Col_Reintentos = string.Empty;
            string Cancelation_json = string.Empty;
            string Tbl_sefclientconf = string.Empty;
            int PeriodoMinutos = 0;
            string OracleConnStr = string.Empty;
            string esquema = string.Empty;
            string querytouse = string.Empty;
            string modulo = string.Empty;
            #endregion

            modulo = "Invalidacion";

            #region conexiondb
            if (_envconfig.GetConnectionString("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                _fileLogger.Log("OracleConnStr: " + OracleConnStr, modulo);
                _logger.LogInformation("OracleConnStr: " + OracleConnStr);
                OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 192.168.139.4)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = XE)));User ID=BHDTE;Password=Sefclient123;";
                //OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 10.10.3.152)(PORT = 6120)))(CONNECT_DATA =(SERVICE_NAME = DESA02)));User ID=OJGARCIA_HIGHTECH;Password=Banco2023;";
                esquema = "BHDTE";

            }
            else
            {
                OracleConnStr = func.OracleConnStr(_envconfig.GetConnectionString("DbHost"), _envconfig.GetConnectionString("DbPort"), _envconfig.GetConnectionString("DbSERVICENAME"), _envconfig.GetConnectionString("DbUserId"), _envconfig.GetConnectionString("DbPassword"));
                esquema = _envconfig.GetConnectionString("Esquema");
            }

            _logger.LogInformation("ConexionDB: " + func.TestConexionDB(OracleConnStr));
            _fileLogger.Log("ConexionDB: " + func.TestConexionDB(OracleConnStr), modulo);
            #endregion

            try
            {

                _fileLogger.Log("Inicio funcion: " + "LeerDocumentosAnuladosAsync " + DateTime.Now.ToString(), modulo);
                _logger.LogInformation("************************inicio funcion LeerDocumentosAnuladosAsync************");
                _logger.LogInformation("*****************************************************************************");
                _logger.LogInformation("Inicio Funcion LeerDocumentosAnuladosAsync" + DateTime.Now.ToString());
                _logger.LogInformation("*****************************************************************************");

                _fileLogger.Log("*****************************************************************************", modulo);
                _fileLogger.Log("Inicio Funcion LeerDocumentosAnuladosAsync" + DateTime.Now.ToString(), modulo);
                _fileLogger.Log("*****************************************************************************", modulo);

                _logger.LogInformation("QueryInvalidate: " + _envconfig.GetConnectionString("QueryInvalidate"));
                _fileLogger.Log(" : " + _envconfig.GetConnectionString("QueryInvalidate"), modulo);
                //_cache.Remove(dt_config);
                dt_config = _cache.GetOrCreate("dt_config", cacheEntry =>
                {
                    return PivoteDataTable.Pivot(func.Get_configuraciones(OracleConnStr, esquema, _logger));
                });


                JConfigString = JsonConvert.SerializeObject(dt_config).Replace("[", "").Replace("]", "");
                dynamic do_config = JObject.Parse(JConfigString);

                #region asignacion_variables
                securityToken.username = do_config["username"];
                securityToken.password = do_config["password"];
                url_security = do_config["url_security"];
                url_reception = do_config["url_reception"];
                url_base = do_config["url_security"];
                url_cancelation = do_config["url_cancelation"];
                Col_IdDocEnc = do_config["Col_IdDocEnc"];
                //Sqlselectdoc = do_config["Sqlselectdocanulado"];
                estado_servicio_invalidate = do_config["estado_servicio_invalidate"];
                Col_TipoDTE = do_config["Col_TipoDTE"];
                Tbl_DocEncabezado = do_config["Tbl_DocEncabezado"];
                Col_resp = do_config["Col_Resp_Invalidate"];
                PeriodoMinutos = do_config["PeriodoSegundosInvalidation"];
                Tbl_log = do_config["Tbl_log"];
                Col_Estado = do_config["Col_Estado"];
                Col_sello = do_config["Col_sello_invalidate"];
                Col_UidDocEnc = do_config["Col_UidDocEnc"];
                Col_Reintentos = do_config["Col_Reintentos"];
                Cancelation_json = do_config["Cancelation_json"];
                Tbl_sefclientconf = do_config["Tbl_sefclientconf"];
                Coldtetype_config = do_config["Coldtetype_config"];
                Company = do_config["Company"];

                #endregion

                _fileLogger.Log(" : " + _envconfig.GetConnectionString("QueryInvalidate"), modulo);

                Sqlselectdoc = func.select_querytouse(OracleConnStr, Tbl_sefclientconf, esquema, _envconfig.GetConnectionString("QueryInvalidate"), _logger, _fileLogger, modulo);

                dtdoc = func.Getdt(OracleConnStr, Sqlselectdoc);
                _fileLogger.Log("Inicio Proceso a las " + DateTime.Now.ToString(), modulo);
                _fileLogger.Log("Inicio Funcion LeerDocumentosAnuladosAsync", modulo);

                // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Proceso", "Inicio Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
                // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Funcion", "Inicio Funcion LeerDocumentosAnuladosAsync " + DateTime.Now.ToString(), "", esquema, "");

                JSONST = JsonConvert.SerializeObject(securityToken);

                respservicio = func.ObtenerOCrearCache(_cache, "resp_tkservice", url_security, JSONST, _logger, _fileLogger, "LeerDocumentosAnuladosAsync", modulo);

                if (respservicio.LastIndexOf("token") > 0)
                {
                    // func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Token", respservicio, "", esquema, "");
                    tk = JObject.Parse(respservicio);
                    token = Convert.ToString(tk["token"]);
                    //_fileLogger.Log("token: " + token);
                }
                else
                {
                    _logger.LogInformation("Error: " + respservicio);
                    _fileLogger.Log("Error: " + respservicio, modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", respservicio, "", esquema, "");
                }

                if (dtdoc.Rows.Count > 0)
                {
                    _logger.LogInformation("Registros a Invalidar: " + dtdoc.Rows.Count.ToString());
                    _fileLogger.Log("Registros a Invalidar: " + dtdoc.Rows.Count.ToString(), modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "LeerDocumentosAnuladosAsync", "Numero de Registros a Invalidar: " + dtdoc.Rows.Count.ToString(), "", esquema, "");
                    for (int i = 0; i < dtdoc.Rows.Count; i++)
                    {
                        dr = dtdoc.Rows[i];
                        idrow = Convert.ToInt32(dr[Col_IdDocEnc].ToString());
                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "P", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAnuladosAsync", modulo);
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "P", idrow.ToString(), esquema, "");
                        _fileLogger.Log("Fila a Procesar: " + dr.ToString(), modulo);

                        dt_invalidation = func.Getdt(OracleConnStr, func.invalidate_event(Tbl_sefclientconf, esquema, Coldtetype_config, Cancelation_json, _logger, _fileLogger, modulo));


                        string l = func.json_invalidation(dt_invalidation, dr);

                        jsonstr = func.fix_json_invalidate(l, _logger, dr[Col_TipoDTE].ToString());

                        string jinvalidation = jsonstr;

                        _logger.LogInformation("JSON ENVIADO: " + jinvalidation);
                        _fileLogger.Log("JSON ENVIADO: " + jinvalidation, modulo);
                        func.update_datos_enc_invalidate(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_IdDocEnc, jsonstr);

                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Json Envio Anulados", jsonstr, idrow.ToString(), esquema, "");
                        if (func.validaJson(jinvalidation) == 1) //json valido
                        {
                            estado_servicio = func.verifica_servicio_reception(url_cancelation);
                            _logger.LogInformation("estado_servicio: " + estado_servicio);
                            _fileLogger.Log("estado_servicio: " + estado_servicio, modulo);
                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado Servicio", estado_servicio, idrow.ToString(), esquema, "");

                            if (estado_servicio == estado_servicio_invalidate)
                            {
                                dynamic data = JObject.Parse(jinvalidation);
                                try
                                {
                                    var resp = func.SendtoInvalidation(data, token, url_cancelation, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                    MHresponse = JsonConvert.SerializeObject(resp);
                                    if (string.IsNullOrEmpty((string)resp.selloRecibido))
                                    {
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "AE", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAnuladosAsync", modulo);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "AE", idrow.ToString(), esquema, "");
                                        _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                        _logger.LogInformation("El registro con id: " + idrow.ToString() + " paso a estado: " + "AE");
                                        _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                        _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                        _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                        _fileLogger.Log("El registro con id: " + idrow.ToString() + " paso a estado: " + "AE", modulo);
                                    }
                                    else
                                    {
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "AF", idrow.ToString(), esquema, "");
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "AF", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAnuladosAsync", modulo);
                                        func.update_sellomh_invalida(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);

                                        _logger.LogInformation("Sello MH: " + (string)resp.selloRecibido);
                                        _fileLogger.Log("Sello MH Invalidacion: " + (string)resp.selloRecibido, modulo);
                                        _fileLogger.Log("El registro con id: " + idrow.ToString() + " paso a estado: " + "AF", modulo);

                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Respuesta MH Invalidacion", (string)resp, idrow.ToString(), esquema, "");
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "AF", idrow.ToString(), esquema, "");


                                        //firmado
                                    }

                                    func.update_datos_enc_resp_invalidate(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_IdDocEnc, MHresponse);

                                }
                                catch (Exception ex)
                                {
                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString(), idrow.ToString(), esquema, "");
                                }
                            }
                        }
                        else
                        {
                            _logger.LogInformation("Error: El json formado " + jinvalidation + " es invalido!!!!");
                            _fileLogger.Log("Error: El json formado " + jinvalidation + " es invalido!!!!", modulo);
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosAnuladosAsync", modulo);
                        }
                        dr = null;
                    }
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Funcion", "Fin Funcion LeerDocumentosAsync " + DateTime.Now.ToString(), idrow.ToString(), esquema, "");
                }
                else
                {
                    _logger.LogInformation("No hay registros a procesar!!!!");
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "No hay registros a procesar!!!! " + DateTime.Now.ToString(), "", esquema, "");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error: " + ex.ToString());
                _fileLogger.Log("Error: " + ex.ToString(), modulo);
                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString() + " " + DateTime.Now.ToString(), "", esquema, "");
            }
            finally
            {
                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "Fin Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
            }

            _logger.LogInformation("Proceso Finalizado!!!!!!!");
            _fileLogger.Log("Proceso Finalizado!!!!!!!", modulo);
            await Task.Delay(TimeSpan.FromMinutes(PeriodoMinutos), stoppingToken);

        }

        public async Task LeerDocumentosReenvioAsync(CancellationToken stoppingToken)
        {

            #region variables
            SEFClientFunctions func = new SEFClientFunctions();
            string jsonstr = string.Empty;
            string jsonstr_apendices = string.Empty;
            dynamic do_json = new JObject();
            dynamic tk = new JObject();
            string token = string.Empty;
            string respservicio = string.Empty;
            DataTable dtdoc = new DataTable();
            DataTable dt_config = new DataTable();
            DataTable dtdetalledoc = new DataTable();
            DataTable dtresumen = new DataTable();
            DataTable dt_apendices = new DataTable();
            DataView dv = new DataView();
            DataRow dr;
            int idrow = 0;
            string estado_servicio = string.Empty;
            Config.SecurityToken securityToken = new Config.SecurityToken();
            string JSONST = string.Empty;
            string JConfigString = string.Empty;
            string url_retrive = string.Empty;
            string url_security = string.Empty;
            string url_base = string.Empty;
            string Col_IdDocEnc = string.Empty;
            string Sqlselectdoc = string.Empty;
            string MHresponse = string.Empty;
            string Col_TipoDTE = string.Empty;
            string Tbl_DocEncabezado = string.Empty;
            string Col_resp = string.Empty;
            string Tbl_log = string.Empty;
            string Col_Estado = string.Empty;
            string Col_sello = string.Empty;
            string Col_UidDocEnc = string.Empty;
            string Col_Reintentos = string.Empty;
            int PeriodoMinutos = 0;
            string Col_TotalVentaGravada = string.Empty;
            string OracleConnStr = string.Empty;
            string esquema = string.Empty;
            int reintentos = 0;
            int Num_Reintentos = 0;
            string Tbl_sefclientconf = string.Empty;
            string Coldtetype_config = string.Empty;
            string SqlApendices = string.Empty;
            string estado_servicio_retrive = string.Empty;
            string querytouse = string.Empty;
            string modulo = string.Empty;
            #endregion

            modulo = "reenvio";

            #region conexiondb
            if (_envconfig.GetConnectionString("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                _fileLogger.Log("OracleConnStr: " + OracleConnStr, modulo);
                _logger.LogInformation("OracleConnStr: " + OracleConnStr);
                OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 192.168.139.4)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = XE)));User ID=BHDTE;Password=Sefclient123;";
                //OracleConnStr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST = 10.10.3.152)(PORT = 6120)))(CONNECT_DATA =(SERVICE_NAME = DESA02)));User ID=OJGARCIA_HIGHTECH;Password=Banco2023;";
                esquema = "BHDTE";

            }
            else
            {
                OracleConnStr = func.OracleConnStr(_envconfig.GetConnectionString("DbHost"), _envconfig.GetConnectionString("DbPort"), _envconfig.GetConnectionString("DbSERVICENAME"), _envconfig.GetConnectionString("DbUserId"), _envconfig.GetConnectionString("DbPassword"));
                esquema = _envconfig.GetConnectionString("Esquema");
            }

            _logger.LogInformation("ConexionDB: " + func.TestConexionDB(OracleConnStr));
            _fileLogger.Log("ConexionDB: " + func.TestConexionDB(OracleConnStr), modulo);
            #endregion

            try
            {


                _fileLogger.Log("Inicio funcion: " + "LeerDocumentosReenvioAsync " + DateTime.Now.ToString(), modulo);
                _logger.LogInformation("************************inicio funcion LeerDocumentosReenvioAsync**************************************");
                _logger.LogInformation("*****************************************************************************");
                _logger.LogInformation("Inicio Funcion LeerDocumentosReenvioAsync" + DateTime.Now.ToString());
                _logger.LogInformation("*****************************************************************************");

                _fileLogger.Log("*****************************************************************************", modulo);
                _fileLogger.Log("Inicio Funcion LeerDocumentosReenvioAsync" + DateTime.Now.ToString(), modulo);
                _fileLogger.Log("*****************************************************************************", modulo);
                _logger.LogInformation("QueryRetrive: " + _envconfig.GetConnectionString("QueryRetrive"));
                _fileLogger.Log("QueryRetrive: " + _envconfig.GetConnectionString("QueryRetrive"), modulo);

                dt_config = _cache.GetOrCreate("dt_config", cacheEntry =>
                {
                    return PivoteDataTable.Pivot(func.Get_configuraciones(OracleConnStr, esquema));
                });

                JConfigString = JsonConvert.SerializeObject(dt_config).Replace("[", "").Replace("]", "");
                dynamic do_config = JObject.Parse(JConfigString);

                #region asignacion_variables
                securityToken.username = do_config["username"];
                securityToken.password = do_config["password"];
                url_security = do_config["url_security"];
                url_base = do_config["url_security"];
                url_retrive = do_config["url_retrive"];
                Col_IdDocEnc = do_config["Col_IdDocEnc"];
                //Sqlselectdoc = do_config["SqlDocRetrives"];
                estado_servicio_retrive = do_config["estado_servicio_retrive"];
                Col_TipoDTE = do_config["Col_TipoDTE"];
                Tbl_DocEncabezado = do_config["Tbl_DocEncabezado"];
                Col_resp = do_config["Col_resp"];
                PeriodoMinutos = do_config["PeriodoMinutosRetrive"];
                Tbl_log = do_config["Tbl_log"];
                Col_Estado = do_config["Col_Estado"];
                Col_sello = do_config["Col_sello"];
                Col_UidDocEnc = do_config["Col_UidDocEnc"];
                Col_Reintentos = do_config["Col_Reintentos"];
                Tbl_sefclientconf = do_config["Tbl_sefclientconf"];
                Coldtetype_config = do_config["Coldtetype_config"];
                #endregion

                Sqlselectdoc = func.select_querytouse(OracleConnStr, Tbl_sefclientconf, esquema, _envconfig.GetConnectionString("QueryRetrive"), _logger, _fileLogger, modulo);

                dtdoc = func.Getdt(OracleConnStr, Sqlselectdoc);
                _fileLogger.Log("Inicio Proceso a las " + DateTime.Now.ToString(), modulo);
                _fileLogger.Log("Inicio Funcion LeerDocumentosReenvioAsync", modulo);

                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Proceso", "Inicio Proceso a las " + DateTime.Now.ToString(), "", esquema, "");
                func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Inicio Funcion", "Inicio Funcion LeerDocumentosReenvioAsync " + DateTime.Now.ToString(), "", esquema, "");

                JSONST = JsonConvert.SerializeObject(securityToken);

                respservicio = func.ObtenerOCrearCache(_cache, "resp_tkservice", url_security, JSONST, _logger, _fileLogger, "LeerDocumentosAnuladosAsync", modulo);

                if (respservicio.LastIndexOf("token") > -1)
                {
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Token", respservicio, "", esquema, "");
                    tk = JObject.Parse(respservicio);
                    token = Convert.ToString(tk["token"]);
                    //_fileLogger.Log("token: " + token);
                }
                else
                {
                    _logger.LogInformation("Error: " + respservicio);
                    _fileLogger.Log("Error: " + respservicio, modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", respservicio, "", esquema, "");
                }

                if (dtdoc.Rows.Count > 0)
                {
                    _logger.LogInformation("Registros a procesar: " + dtdoc.Rows.Count.ToString());
                    _fileLogger.Log("Registros a procesar: " + dtdoc.Rows.Count.ToString(), modulo);
                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "LeerDocumentosReenvioAsync", "Numero de Registros a Procesar: " + dtdoc.Rows.Count.ToString(), "", esquema, "");

                    for (int i = 0; i < dtdoc.Rows.Count; i++)
                    {
                        dr = dtdoc.Rows[i];
                        idrow = Convert.ToInt32(dr[Col_IdDocEnc].ToString());
                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "P", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "P", idrow.ToString(), esquema, "");
                        _fileLogger.Log("Fila a Procesar: " + dr.ToString(), modulo);
                        _logger.LogInformation("IdRegistro: " + idrow);
                        _fileLogger.Log("IdRegistro: " + idrow, modulo);
                        try
                        {
                            dynamic do_json1 = new JObject();
                            do_json1.Add("codigoGeneracion", dr[Col_UidDocEnc].ToString());
                            jsonstr = JsonConvert.SerializeObject(do_json1);

                        }
                        catch (Exception ex)
                        {
                            if (ex.ToString().Contains("Newtonsoft.Json.JsonReaderException"))
                            {
                                int posini = ex.ToString().IndexOf("Path") + 5;
                                _fileLogger.Log("posini: " + posini.ToString(), modulo);
                                _fileLogger.Log("posini: " + posini.ToString(), modulo);
                                string tmp = ex.ToString().Substring(posini);
                                _fileLogger.Log("tmp: " + tmp, modulo);
                                int posfinal = tmp.ToString().IndexOf(",");
                                _fileLogger.Log("posfinal: " + posfinal.ToString(), modulo);

                                string campo = tmp.Substring(0, posfinal);
                                _logger.LogInformation("campo: " + campo);
                                _fileLogger.Log("campo: " + campo, modulo);
                                _fileLogger.Log("Error GeneraNodoRootJsonDinamico: No se pudo formar bien el JSON, problemas de datos en el campo: " + campo, modulo);
                                _logger.LogInformation("Error GeneraNodoRootJsonDinamico: No se pudo formar bien el JSON, problemas de datos en el campo: " + campo);

                            }
                            else
                            {
                                _fileLogger.Log("Error GeneraNodoRootJsonDinamico: " + ex.ToString(), modulo);
                                _logger.LogInformation("Error GeneraNodoRootJsonDinamico: " + ex.ToString());
                            }
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                            break;
                        }

                        _fileLogger.Log("JSON RETRIVE: " + jsonstr, modulo);

                        do_json = JObject.Parse(jsonstr);

                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Json Envio Reenvio", jsonstr, idrow.ToString(), esquema, "");

                        _logger.LogInformation("Json a Procesar: " + jsonstr);

                        if (func.validaJson(jsonstr) == 1) //json valido
                        {
                            estado_servicio = func.verifica_servicio_reception(url_retrive);
                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado Servicio", estado_servicio, idrow.ToString(), esquema, "");
                            _logger.LogInformation("El estado del servicio: " + url_retrive + " es: " + estado_servicio + " se compara con el estado configurado en la tabla: " + estado_servicio_retrive);
                            _fileLogger.Log("El estado del servicio: " + url_retrive + " es: " + estado_servicio + " se compara con el estado configurado en la tabla: " + estado_servicio_retrive, modulo);
                            if (estado_servicio == estado_servicio_retrive)
                            {
                                var resp = (dynamic)null;
                                dynamic data = JObject.Parse(jsonstr);
                                try
                                {
                                    resp = func.SendtoRetrive(data, token, url_retrive, do_config, OracleConnStr, esquema, _logger, _fileLogger, modulo);
                                    MHresponse = JsonConvert.SerializeObject(resp);
                                    if (string.IsNullOrEmpty((string)resp.selloRecibido))
                                    {
                                        if (((string)resp.estado == "RECHAZADO" && (string)resp.descripcionMsg == "Documento no se encuentra en SEF") || ((string)resp.estado == "RECHAZADO" && ((string)resp.descripcionMsg).Contains("Error")) || ((string)resp.estado == "RECHAZADO" && ((string)resp.descripcionMsg).Contains("Soporte")))
                                        {
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CH", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                                            _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                            _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                            _logger.LogInformation("Respuesta MH: " + MHresponse);
                                            _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                            _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                            _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                        }
                                        else
                                        {
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoRetrive", (string)resp.estado, idrow.ToString(), esquema, "");
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoRetrive", (string)resp.descripcionMsg, idrow.ToString(), esquema, "");
                                            func.insert_Arraylogtbl(OracleConnStr, Tbl_log, esquema, "Resp_SendtoRetrive", (string[])resp.observaciones, idrow.ToString(), esquema, "");
                                            func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "EC", idrow.ToString(), esquema, "");
                                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "EC", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                                            _logger.LogInformation("Estado Registro: " + (string)resp.estado);
                                            _logger.LogInformation("Mensaje Error: " + (string)resp.descripcionMsg);
                                            _logger.LogInformation("Respuesta MH: " + MHresponse);
                                            _fileLogger.Log("Estado Registro: " + (string)resp.estado, modulo);
                                            _fileLogger.Log("Mensaje Error: " + (string)resp.descripcionMsg, modulo);
                                            _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                        }

                                    }
                                    else
                                    {
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "CF", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                                        func.update_sellomh(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_sello, (string)resp.selloRecibido, Col_IdDocEnc);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "SelloMH", "El registro con id: " + idrow.ToString() + " Tiene el Sello: " + (string)resp.selloRecibido, idrow.ToString(), esquema, "");
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Estado", "El registro con id: " + idrow.ToString() + " paso a estado: " + "FC", idrow.ToString(), esquema, "");
                                        _logger.LogInformation("SelloMH: " + (string)resp.selloRecibido);
                                        _logger.LogInformation("Respuesta MH: " + MHresponse);
                                        _fileLogger.Log("SelloMH: " + (string)resp.selloRecibido, modulo);
                                        _fileLogger.Log("Respuesta MH: " + MHresponse, modulo);
                                        //firmado
                                    }
                                    func.update_mh_resp(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_resp, MHresponse, Col_IdDocEnc);
                                    func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Respuesta MH", MHresponse, idrow.ToString(), esquema, "");

                                }
                                catch (Exception ex)
                                {
                                    if (string.IsNullOrEmpty(resp))
                                    {
                                        _logger.LogInformation("Error: Respuesta Vacia el MH esta Saturado");
                                        _fileLogger.Log("Error: Respuesta Vacia el MH esta Saturado", modulo);
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "EC", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                                    }
                                    else
                                    {
                                        _logger.LogInformation("Error: " + ex.ToString());
                                        _fileLogger.Log("Error: " + ex.ToString(), modulo);
                                        func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                                        func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Error", ex.ToString(), idrow.ToString(), esquema, "");
                                    }

                                }
                            }
                        }
                        else
                        {
                            _logger.LogInformation("Error: JSON Invalido");
                            _fileLogger.Log("Error: JSON Invalido", modulo);
                            func.update_estado(OracleConnStr, Tbl_DocEncabezado, idrow, esquema, Col_Estado, "E", Col_IdDocEnc, _fileLogger, _logger, "LeerDocumentosReenvioAsync", modulo);
                        }
                        dr = null;
                    }
                    //  func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Funcion", "Fin Funcion LeerDocumentosAsync " + DateTime.Now.ToString(), idrow.ToString(), esquema, "");
                }
                else
                {
                    _logger.LogInformation("No hay registros a procesar!!!!");
                    _logger.LogInformation("************************Fin Funucion**************************************");
                    _fileLogger.Log("No hay registros a procesar!!!!", modulo);
                    _fileLogger.Log("************************Fin Funucion**************************************", modulo);
                    //  func.insert_logtbl(OracleConnStr, Tbl_log, esquema, "Fin Proceso", "No hay registros a procesar!!!! " + DateTime.Now.ToString(), "", esquema, "");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error: " + ex.ToString());
                if (ex.ToString().Contains("System.NullReferenceException"))
                {
                    _logger.LogInformation("Error: " + ex.ToString());
                    _fileLogger.Log("Error: " + ex.ToString(), modulo);
                }
            }
            finally
            {
                _logger.LogInformation("************************Fin Proceso**************************************");
                _fileLogger.Log("************************Fin Proceso**************************************", modulo);

            }


            await Task.Delay(TimeSpan.FromMinutes(PeriodoMinutos), stoppingToken);

        }
    }
}
