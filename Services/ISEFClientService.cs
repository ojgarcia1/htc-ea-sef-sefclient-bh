﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.Services
{
    public interface ISEFClientService
    {
        Task LeerDocumentosAsync(CancellationToken stoppingToken);

        Task LeerDocumentosContingencyAsync(CancellationToken stoppingToken);

        Task LeerDocumentosAnuladosAsync(CancellationToken stoppingToken);

        Task LeerDocumentosReenvioAsync(CancellationToken stoppingToken);

    }
}
