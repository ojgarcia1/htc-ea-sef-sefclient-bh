﻿using App.WindowsService.DTE;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using Newtonsoft.Json.Linq;
using App.WindowsService.log.LogWriter;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System.Collections.Generic;


namespace App.WindowsService.Services
{
    public class SEFClientFunctions
    {

        #region DataTable
        public DataTable Getdt(string connstr, string selectstr)
        {
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = selectstr;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();
            }
            catch
            {

            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return dt;
        }

        public string GetSello(string connstr, string esquema, string Col_sello, string tabla, string Col_codgen, string valcodgen)
        {
            string selectstr = string.Empty;
            string sello = string.Empty;
            selectstr = "SELECT " + Col_sello + " FROM " + esquema + "." + tabla + " where " + Col_codgen + " = " + "'" + valcodgen + "'";
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = selectstr;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();


                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sello = dr[0].ToString();
                    }
                }
            }
            catch
            {
                sello = string.Empty;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return sello;
        }

        public string select_querytouse(string connstr, string tabla, string esquema, string queryname, ILogger log, DailyLogFileWriter filelog, string modulo)
        {
            string val = string.Empty;
            string select_str = string.Empty;
            select_str = " SELECT VAL";
            select_str += " FROM " + esquema + "." + tabla;
            select_str += " WHERE ACTIVEFLG = 'Y' ";
            select_str += " AND NAME=" + "'" + queryname + "'";


            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);

            log.LogInformation("Query a utilizar: " + select_str);
            filelog.Log("Query a utilizar: " + select_str, modulo);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = select_str;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    val = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                log.LogInformation("Error: " + ex.ToString());
                filelog.Log("Query a utilizar: " + ex.ToString(), modulo);
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return val;
        }

        public string prop_valor(string connstr, string selectstr)
        {
            string val = string.Empty;
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);

            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = selectstr;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    val = dt.Rows[0][0].ToString();
                }
            }
            catch
            {

            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return val;
        }

        public DataTable Get_configuraciones(string connstr, string esquema, ILogger log)
        {
            DataTable dt = new DataTable();

            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            string sql_select = string.Empty;
            sql_select = "SELECT NAME, VAL ";
            sql_select += " FROM " + esquema + ".SEFCLIENT_CONF";
            sql_select += " WHERE TYPE = " + "'" + "CONFIGURACION" + "'";

            log.LogInformation("sql_select: " + sql_select);
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sql_select;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return dt;
        }

        #endregion

        #region QUERYS GET INFO Tabla Configuracion
        public string select_config_str(string tipo, string subtipo, string parent_id, string esquema)
        {
            string select_str = string.Empty;
            select_str = "SELECT name, val,DATATYPE,ISCOLUMN,IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE type = '" + tipo + "'";
            select_str += " AND SUBTYPE = '" + subtipo + "'";
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " AND PARIDSEFCONF = " + parent_id;
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_str_array_valor(string tipo, string subtipo, string esquema, string val, string dte_tipo)
        {
            string select_str = string.Empty;
            select_str = "SELECT name, val,DATATYPE,ISCOLUMN,IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE type = '" + tipo + "'";
            select_str += " AND SUBTYPE = '" + subtipo + "'";
            select_str += " AND VAL = '" + val + "'";
            select_str += " AND DTETYPE = '" + dte_tipo + "'";
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_valor(string name, string tipo, string subtipo, string esquema)
        {
            string select_str = string.Empty;
            select_str = "SELECT VAL";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE NAME = '" + name + "'";
            select_str += " AND type = '" + tipo + "'";
            if (!string.IsNullOrEmpty(subtipo))
            {
                select_str += " AND SUBTYPE = '" + subtipo + "'";
            }
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_id(string name, string tipo, string subtipo, string esquema)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE NAME = '" + name + "'";
            select_str += " AND type = '" + tipo + "'";
            if (!string.IsNullOrEmpty(subtipo))
            {
                select_str += " AND SUBTYPE = '" + subtipo + "'";
            }
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_id_val(string val, string tipo, string subtipo, string esquema)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE VAL = '" + val + "'";
            select_str += " AND type = '" + tipo + "'";
            if (!string.IsNullOrEmpty(subtipo))
            {
                select_str += " AND SUBTYPE = '" + subtipo + "'";
            }
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_id_val_dtetype(string val, string tipo, string subtipo, string esquema, string tipo_dte)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE VAL = '" + val + "'";
            select_str += " AND type = '" + tipo + "'";
            if (!string.IsNullOrEmpty(subtipo))
            {
                select_str += " AND SUBTYPE = '" + subtipo + "'";
            }
            select_str += " AND DTETYPE = '" + tipo_dte + "'";
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_config_id_val_parent(string val, string tipo, string subtipo, string esquema, string parent_id)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE VAL = '" + val + "'";
            select_str += " AND type = '" + tipo + "'";
            if (!string.IsNullOrEmpty(subtipo))
            {
                select_str += " AND SUBTYPE = '" + subtipo + "'";
            }
            select_str += " AND PARIDSEFCONF = " + parent_id;
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_documentos(string name, string tipo, string subtipo, string esquema)
        {
            string select_str = string.Empty;
            select_str = "SELECT val";
            select_str += " FROM " + esquema + ".sefclient_conf";
            select_str += " WHERE NAME = '" + name + "'";
            select_str += " AND type = '" + tipo + "'";
            select_str += " AND SUBTYPE = '" + subtipo + "'";
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " ORDER BY ORDERBY";

            return select_str;
        }

        public string select_tributos(string tabla, string esquema, string tributos, string mostrar)
        {
            string select_str = string.Empty;
            select_str = "SELECT NAME, VAL, DESCTEXT";
            select_str += " FROM " + esquema + "." + tabla;
            select_str += " WHERE SUBTYPE = 'CAT-015'";
            select_str += " AND PROCESSNAME = '" + mostrar + "'";
            select_str += " AND ACTIVEFLG = 'Y'";
            select_str += " AND NAME IN (" + tributos + ")";


            return select_str;
        }

        public string select_apendices(string tabla, string esquema, string col_tdet, string tipodte)
        {
            string select_str = string.Empty;
            select_str = " SELECT VAL AS VALOR, NAME AS CAMPO, DESCTEXT AS ETIQUETA ";
            select_str += " FROM " + esquema + "." + tabla;
            select_str += " WHERE ACTIVEFLG = 'Y' ";
            select_str += " AND " + col_tdet + "= '" + tipodte + "'";
            select_str += " AND SUBTYPE = 'apendice'";

            return select_str;
        }

        public string invalidate_event(string tabla, string esquema, string col_tdet, string tipodte, ILogger log, DailyLogFileWriter logfile, string modulo)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF,VAL,DATATYPE,SUBTYPE, PARIDSEFCONF,NAME,ISCOLUMN";
            select_str += " FROM " + esquema + "." + tabla;
            select_str += " WHERE ACTIVEFLG = 'Y'";
            select_str += " AND SUBTYPE in  (SELECT SUBTYPE FROM " + esquema + "." + tabla + " where ACTIVEFLG = 'Y' and type = 'JSON' AND " + col_tdet + " = '" + tipodte + "' group by SUBTYPE)";
            select_str += " AND " + col_tdet + " = '" + tipodte + "'";
            select_str += " ORDER BY ORDERBY";

            logfile.Log("SQL QUERY INVALIDACION: " + select_str, modulo);
            log.LogInformation("SQL QUERY INVALIDACION: " + select_str);
            return select_str;
        }

        public string jcontingency_event(string tabla, string esquema, string col_tdet, string tipodte)
        {
            string select_str = string.Empty;
            select_str = "SELECT IDSEFCONF,VAL,DATATYPE,SUBTYPE, PARIDSEFCONF,NAME,ISCOLUMN";
            select_str += " FROM " + esquema + "." + tabla;
            select_str += " WHERE ACTIVEFLG = 'Y'";
            select_str += " AND SUBTYPE in  (SELECT SUBTYPE FROM " + esquema + "." + tabla + " where ACTIVEFLG = 'Y' and type = 'JSON' AND " + col_tdet + " = '" + tipodte + "' group by SUBTYPE)";
            select_str += " AND " + col_tdet + " = '" + tipodte + "'";
            select_str += " ORDER BY ORDERBY";


            return select_str;
        }



        #endregion

        #region JSON_DINAMICO
        public string GeneraJson(DataRow drp, DataTable dt_detalle, DataTable dt_resumen)
        {
            string JSONresult = string.Empty;
            Guid codigoGeneracion = Guid.NewGuid();
            dte_fac.Rootobject dtefac_root = new dte_fac.Rootobject();
            dte_fac.Identificacion dtefac_identificacion = new dte_fac.Identificacion();
            dte_fac.Emisor dte_fac_emisor = new dte_fac.Emisor();
            dte_fac.Receptor dte_fac_receptor = new dte_fac.Receptor();
            dte_fac.CuerpoDocumento[] dte_fac_cuerpoDocumento = new dte_fac.CuerpoDocumento[dt_detalle.Rows.Count];
            dte_fac.Resumen dte_fac_resumen = new dte_fac.Resumen();
            dte_fac.Extension dte_fac_extension = new dte_fac.Extension();
            dte_fac.Apendice dte_fac_apendice = new dte_fac.Apendice();
            dte_fac.OtrosDocumentos dte_fac_otrosDocumentos = new dte_fac.OtrosDocumentos();
            dte_fac.VentaTercero dte_fac_ventaTercero = new dte_fac.VentaTercero();
            dte_fac.Direccion dte_fac_Direccion_emisor = new dte_fac.Direccion();
            dte_fac.Direccion dte_fac_Direccion_receptor = new dte_fac.Direccion();
            dte_fac.Tributos dte_fac_tributos = new dte_fac.Tributos();
            dte_fac.Medico dte_fac_otrosDocumentos_medico = new dte_fac.Medico();

            //nodo identificacion
            dtefac_identificacion.version = 1;
            dtefac_identificacion.ambiente = "00";
            dtefac_identificacion.tipoDte = drp["TIPD_INTERNO"].ToString();
            dtefac_identificacion.numeroControl = drp["NUM_CTRL"].ToString();
            dtefac_identificacion.codigoGeneracion = drp["COD_GEN"].ToString();
            dtefac_identificacion.tipoModelo = Convert.ToInt16(drp["MOD_FACTURACION"].ToString());
            dtefac_identificacion.tipoOperacion = Convert.ToInt16(drp["TIPO_TRANS"].ToString());
            dtefac_identificacion.tipoContingencia = string.IsNullOrEmpty(drp["TIP_CONT"].ToString()) ? null : drp["TIP_CONT"].ToString();
            dtefac_identificacion.motivoContin = string.IsNullOrEmpty(drp["MOT_CONT"].ToString()) ? null : drp["MOT_CONT"].ToString();
            dtefac_identificacion.fecEmi = DateTime.Now.ToString("yyyy-MM-dd");
            dtefac_identificacion.horEmi = null;
            dtefac_identificacion.tipoMoneda = "USD";

            //nodo emisor
            dte_fac_emisor.code = drp["COD_E"].ToString();
            //dte_fac_emisor.codeSucursal = drp["SRC_BRANCH"].ToString();
            dte_fac_emisor.nit = drp["NIT_E"].ToString();
            dte_fac_emisor.nrc = drp["NRC_E"].ToString();
            dte_fac_emisor.nombre = drp["NOM_RS_E"].ToString();
            dte_fac_emisor.codActividad = drp["ACT_ECO_E"].ToString();
            dte_fac_emisor.descActividad = "prueba";
            dte_fac_emisor.nombreComercial = drp["NUM_COM_E"].ToString();
            dte_fac_emisor.tipoEstablecimiento = string.IsNullOrEmpty("TIPO_ESTB_E") ? null : drp["TIPO_ESTB_E"].ToString();
            dte_fac_emisor.telefono = string.IsNullOrEmpty("TEL_E") ? null : drp["TEL_E"].ToString();
            dte_fac_emisor.correo = string.IsNullOrEmpty("EMAIL_E") ? null : drp["EMAIL_E"].ToString();
            dte_fac_emisor.codPuntoVentaMH = string.IsNullOrEmpty("COD_PV_MH") ? null : drp["COD_PV_MH"].ToString();
            //direccion emisor
            dte_fac_Direccion_emisor.departamento = drp["DEPTO_E"].ToString();
            dte_fac_Direccion_emisor.municipio = drp["MUNI_E"].ToString();
            dte_fac_Direccion_emisor.complemento = drp["DIR_E"].ToString();
            dte_fac_emisor.direccion = dte_fac_Direccion_emisor;

            //receptor
            //dte_fac_receptor.nit = drp["NIT_R"].ToString();
            dte_fac_receptor.nrc = null; //drp["NRC_R"].ToString();
            dte_fac_receptor.nombre = drp["NOM_RS_R"].ToString();
            dte_fac_receptor.tipoDocumento = null;//drp["TIPOD_IDEN_R"].ToString().Trim();
            dte_fac_receptor.numDocumento = drp["NUMD_IDEN_R"].ToString().PadLeft(20, '0');
            dte_fac_receptor.codActividad = drp["ACT_ECO_R"].ToString();
            dte_fac_receptor.descActividad = "prueba";
            //dte_fac_receptor.nombreComercial = drp["NOM_COM_R"].ToString();

            //direccion receptor
            dte_fac_Direccion_receptor.departamento = drp["DEPTO_R"].ToString().Trim();
            dte_fac_Direccion_receptor.municipio = drp["MUNI_R"].ToString().Trim();
            dte_fac_Direccion_receptor.complemento = drp["DIR_R"].ToString().Trim();
            dte_fac_receptor.direccion = dte_fac_Direccion_receptor;

            dte_fac_receptor.telefono = drp["TEL_R"].ToString();
            dte_fac_receptor.correo = drp["EMAIL_R"].ToString();
            // dte_fac_receptor.codPais = drp["CPAIS_R"].ToString();
            //dte_fac_receptor.nombrePais = drp["NPAIS_R"].ToString();
            // dte_fac_receptor.tipoPersona = Convert.ToInt16(drp["TP_R"].ToString());


            //otrosdDocumentos
            dte_fac_otrosDocumentos.codDocAsociado = "00000000000000";
            dte_fac_otrosDocumentos.descDocumento = "0000000";
            dte_fac_otrosDocumentos.detalleDocumento = null;
            //otrosdDocumentos-medico
            dte_fac_otrosDocumentos_medico.nit = null;
            dte_fac_otrosDocumentos_medico.docIdentificacion = null;
            dte_fac_otrosDocumentos_medico.tipoServicio = 0;
            dte_fac_otrosDocumentos.medico = dte_fac_otrosDocumentos_medico;

            //nodo ventaTercero
            if (string.IsNullOrEmpty(drp["NIT_T"].ToString()))
            {
                dte_fac_ventaTercero = null;
            }
            else
            {
                dte_fac_ventaTercero.nit = drp["NIT_T"].ToString();
                dte_fac_ventaTercero.nombre = drp["NOM_RS_T"].ToString();
            }


            //cuerpoDocumento
            if (dt_detalle.Rows.Count > 0)
            {
                for (int t = 0; t < dt_detalle.Rows.Count; t++)
                {
                    dte_fac_cuerpoDocumento[t] = new dte_fac.CuerpoDocumento();
                    dte_fac_cuerpoDocumento[t].numItem = Convert.ToInt32(dt_detalle.Rows[t]["NUM_ITEM"].ToString());
                    dte_fac_cuerpoDocumento[t].tipoItem = Convert.ToInt32(dt_detalle.Rows[t]["TIPO_ITEM"].ToString());
                    dte_fac_cuerpoDocumento[t].numeroDocumento = null;
                    dte_fac_cuerpoDocumento[t].codigo = string.IsNullOrEmpty(dt_detalle.Rows[t]["CODIGO_ITEM"].ToString()) ? null : dt_detalle.Rows[t]["CODIGO_ITEM"].ToString();
                    dte_fac_cuerpoDocumento[t].codTributo = null;
                    dte_fac_cuerpoDocumento[t].descripcion = dt_detalle.Rows[t]["DESCRIPCION"].ToString();
                    dte_fac_cuerpoDocumento[t].cantidad = Convert.ToInt32(dt_detalle.Rows[t]["CANTIDAD"].ToString());
                    //dte_fac_cuerpoDocumento[t].uniMedida = Convert.ToInt32(dt_detalle.Rows[t]["unidad"].ToString());
                    dte_fac_cuerpoDocumento[t].uniMedida = Convert.ToInt32(dt_detalle.Rows[t]["UNIDAD"].ToString());
                    dte_fac_cuerpoDocumento[t].precioUni = Convert.ToDecimal(dt_detalle.Rows[t]["P_UNI"].ToString());
                    dte_fac_cuerpoDocumento[t].montoDescu = Convert.ToDecimal(dt_detalle.Rows[t]["DESCUENTO"].ToString());
                    dte_fac_cuerpoDocumento[t].ventaNoSuj = Convert.ToDecimal(dt_detalle.Rows[t]["V_NO_SUJETAS"].ToString());
                    dte_fac_cuerpoDocumento[t].ventaExenta = Convert.ToDecimal(dt_detalle.Rows[t]["V_EXENTAS"].ToString());
                    dte_fac_cuerpoDocumento[t].ventaGravada = Convert.ToDecimal(dt_detalle.Rows[t]["V_GRAVADAS"].ToString());
                    dte_fac_cuerpoDocumento[t].psv = 0; //que campo es
                    dte_fac_cuerpoDocumento[t].noGravado = 0; // que campo es
                    //dte_fac_cuerpoDocumento[t].tipoDte = drp["TIPD_INTERNO"].ToString();
                    //dte_fac_cuerpoDocumento[t].tipoGeneracion = 0;
                    //dte_fac_cuerpoDocumento[t].fechaGeneracion = DateAndTime.Now.ToString("yyyy-MM-dd");
                    //dte_fac_cuerpoDocumento[t].exportaciones = Convert.ToDecimal(string.IsNullOrEmpty(dt_detalle.Rows[t]["EXPOR"].ToString())?"0": dt_detalle.Rows[t]["EXPOR"].ToString());
                    int[] trib = new int[] { 90 };
                    dte_fac_cuerpoDocumento[t].tributos = null; //averiguar
                    dte_fac_cuerpoDocumento[t].ivaItem = Convert.ToDecimal(dt_detalle.Rows[t]["IVA_RETENIDO"].ToString());
                    //dte_fac_cuerpoDocumento[t].obsItem = dt_detalle.Rows[t]["OBSERVACION"].ToString();
                    //dte_fac_cuerpoDocumento[t].tipoDoc = 0;//averiguar
                    //dte_fac_cuerpoDocumento[t].numDocumento = drp["COD_GEN"].ToString();
                    //dte_fac_cuerpoDocumento[t].fechaEmision = DateAndTime.Now.ToString("yyyy-MM-dd");
                    //dte_fac_cuerpoDocumento[t].montoSujetoGrav = Convert.ToDecimal(string.IsNullOrEmpty(dt_detalle.Rows[t]["MON_PERCEP_SIN_IVA"].ToString()) ? "0" : dt_detalle.Rows[t]["MON_PERCEP_SIN_IVA"].ToString());
                    //dte_fac_cuerpoDocumento[t].codigoRetencionMH = "0"; //falta en tabla
                    //dte_fac_cuerpoDocumento[t].ivaRetenido = Convert.ToDecimal(dt_detalle.Rows[t]["IVA_RETENIDO"].ToString());

                }
            }
            //resumen
            if (dt_resumen.Rows.Count > 0)
            {
                for (int g = 0; g < dt_resumen.Rows.Count; g++)
                {
                    dte_fac_resumen.totalNoSuj = Convert.ToDecimal(dt_resumen.Rows[g]["TOT_V_NO_SUJETA"].ToString());
                    dte_fac_resumen.totalExenta = Convert.ToDecimal(dt_resumen.Rows[g]["TOT_V_EXENTA"].ToString());
                    dte_fac_resumen.totalGravada = Convert.ToDecimal(dt_resumen.Rows[g]["TOT_V_GRAVADA"].ToString());
                    //dte_fac_resumen.totalExportacion = Convert.ToDecimal(dt_resumen.Rows[g]["total_venta_no_sujeta"].ToString());
                    dte_fac_resumen.subTotalVentas = Convert.ToDecimal(dt_resumen.Rows[g]["SUBTOTAL"].ToString());
                    dte_fac_resumen.descuNoSuj = 0;
                    dte_fac_resumen.descuExenta = 0;
                    dte_fac_resumen.descuGravada = 0;
                    dte_fac_resumen.porcentajeDescuento = 0;
                    dte_fac_resumen.totalDescu = 0;
                    dte_fac_resumen.tributos = null;
                    dte_fac_resumen.subTotal = Convert.ToDecimal(dt_resumen.Rows[g]["SUBTOTAL"].ToString());
                    //dte_fac_resumen.ivaPerci1 = Convert.ToDecimal(dt_resumen.Rows[g]["IVA_PERCIBIDO"].ToString());
                    dte_fac_resumen.ivaRete1 = Convert.ToDecimal(dt_resumen.Rows[g]["IVA_RETENIDO"].ToString());
                    dte_fac_resumen.reteRenta = 0;
                    //dte_fac_resumen.total = Convert.ToDecimal(dt_resumen.Rows[g]["TOT_GENERAL"].ToString()); 
                    dte_fac_resumen.montoTotalOperacion = Convert.ToDecimal(dt_resumen.Rows[g]["MON_OP"].ToString());
                    dte_fac_resumen.totalNoGravado = 0;
                    dte_fac_resumen.totalPagar = 0;
                    dte_fac_resumen.totalLetras = dt_resumen.Rows[g]["MONL"].ToString();
                    dte_fac_resumen.totalIva = 0;
                    dte_fac_resumen.saldoFavor = 0;
                    dte_fac_resumen.condicionOperacion = Convert.ToInt32(dt_resumen.Rows[g]["CON_OPE"].ToString());
                    //dte_fac_resumen.codIncoterms = "";
                    //dte_fac_resumen.descIncoterms = "";
                    //dte_fac_resumen.observaciones = dt_resumen.Rows[g]["OBSERVACION"].ToString();
                    dte_fac_resumen.numPagoElectronico = "";
                    //dte_fac_resumen.descuento = 0;
                    // dte_fac_resumen.totalSujetoRetencion = 0;
                    //dte_fac_resumen.totalIVAretenido = 0;
                }

            }


            //extension
            dte_fac_extension.nombEntrega = string.IsNullOrEmpty(drp["NUM_COM_E"].ToString()) ? null : drp["NUM_COM_E"].ToString();
            dte_fac_extension.docuEntrega = string.IsNullOrEmpty(drp["NIT_E"].ToString()) ? null : drp["NIT_E"].ToString();
            dte_fac_extension.nombRecibe = string.IsNullOrEmpty(drp["NOM_COM_R"].ToString()) ? null : drp["NOM_COM_R"].ToString();
            dte_fac_extension.docuRecibe = string.IsNullOrEmpty(drp["NUMD_IDEN_R"].ToString()) ? null : drp["NUMD_IDEN_R"].ToString();
            dte_fac_extension.observaciones = null;
            dte_fac_extension.placaVehiculo = null;

            //apendice
            dte_fac_apendice.campo = "imprimir"; //averiguar posibles valores
            dte_fac_apendice.etiqueta = "Print"; //averiguar posibles valores
            dte_fac_apendice.valor = "OK"; //averiguar posibles valores

            //nodo root
            dtefac_root.identificacion = dtefac_identificacion;
            dtefac_root.documentoRelacionado = null;
            dtefac_root.emisor = dte_fac_emisor;
            dtefac_root.receptor = dte_fac_receptor;
            dtefac_root.otrosDocumentos = null;
            dtefac_root.ventaTercero = dte_fac_ventaTercero;
            dtefac_root.cuerpoDocumento = dte_fac_cuerpoDocumento;
            dtefac_root.resumen = dte_fac_resumen;
            dtefac_root.extension = dte_fac_extension;
            //dtefac_root.apendice = dte_fac_apendice;
            dtefac_root.apendice = null;


            JSONresult = JsonConvert.SerializeObject(dtefac_root);

            return JSONresult;
        }

        public int validaJson(string strJson)
        {
            int resp = 0;
            try
            {
                var tmpObj = JsonValue.Parse(strJson);
                resp = 1;
            }
            catch (FormatException fex)
            {
                //Invalid json format
                resp = 0;
                return resp;
            }
            catch (Exception ex) //some other exception
            {
                return resp;
            }
            return resp;
        }

        public string apendices_json(string doc, ILogger log, DataTable dt)
        {
            JObject docJObject = JObject.Parse(doc);

            JArray JAapendices = new JArray();

            foreach (DataRow row in dt.Rows)
            {
                JObject japendices = new JObject();
                japendices.Add("campo", row["CAMPO"].ToString());
                japendices.Add("etiqueta", row["ETIQUETA"].ToString());
                japendices.Add("valor", row["VALOR"].ToString());

                JAapendices.Add(japendices);
            }

            docJObject.SelectToken("apendice").Replace(JAapendices);



            //JObject receptorJson = docJObject["receptor"];

            //log.LogInformation("Dui Sin Formato: " + dui);
            //dynamic contactExists = JsonConvert.DeserializeObject(docJObject);
            // var tipodoc = receptorJson.SelectToken("tipoDocumento");
            //var tipodoc = docJObject["receptor"]["tipoDocumento"];
            //_ = (string)dataJson["tipoDocumento"];
            //dui = (string)dataJson["numDocumento"];

            string x = JsonConvert.SerializeObject(docJObject);

            return x;

        }

        public string json_invalidation(DataTable dt, DataRow dr)
        {
            DataTable dt_filter = new DataTable();
            DataTable dt_filter_hijo = new DataTable();
            DataView dv_root = new DataView(dt);
            DataView dv_hijo = new DataView(dt);
            dv_root.RowFilter = "SUBTYPE='root'";
            dt_filter = dv_root.ToTable();
            string datatype = string.Empty;
            int nvalor = 0;
            string svalor = string.Empty;
            string propiedad = string.Empty;
            string iscolumn = string.Empty;
            string columna = string.Empty;
            dynamic do_jarray = new JArray();
            dynamic do_json = new JObject();

            foreach (DataRow row in dt_filter.Rows)
            {
                dynamic do_json_hijo = new JObject();
                var path = row["VAL"].ToString();
                do_json.Add(path, null);

                dv_hijo.RowFilter = "SUBTYPE=" + "'" + row["VAL"].ToString() + "'";
                dt_filter_hijo = dv_hijo.ToTable();
                foreach (DataRow row1 in dt_filter_hijo.Rows)
                {
                    datatype = row1["DATATYPE"].ToString();
                    propiedad = row1["NAME"].ToString();
                    svalor = row1["VAL"].ToString();
                    columna = row1["VAL"].ToString();
                    iscolumn = row1["ISCOLUMN"].ToString();

                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                    {
                        do_json_hijo.Add(propiedad, "null");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(iscolumn) || string.IsNullOrWhiteSpace(iscolumn))
                        {
                            switch (datatype)
                            {
                                case "number":
                                    nvalor = Convert.ToInt16(svalor);
                                    do_json_hijo.Add(propiedad, nvalor);
                                    break;

                                case "string":
                                    do_json_hijo.Add(propiedad, svalor);
                                    break;
                            }
                        }
                        else
                        {
                            switch (datatype)
                            {
                                case "number":
                                    svalor = dr[columna].ToString();
                                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                                    {
                                        nvalor = Convert.ToInt16("0");
                                        do_json_hijo.Add(propiedad, nvalor);
                                    }
                                    else
                                    {
                                        nvalor = Convert.ToInt16(svalor);
                                        do_json_hijo.Add(propiedad, nvalor);
                                    }

                                    break;

                                case "string":
                                    svalor = dr[columna].ToString();
                                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                                    {
                                        do_json_hijo.Add(propiedad, null);
                                    }
                                    else
                                    {
                                        do_json_hijo.Add(propiedad, svalor);
                                    }

                                    break;
                                default:
                                    svalor = "null";
                                    do_json_hijo.Add(propiedad, svalor);
                                    break;
                            }
                        }
                    }

                }
                do_json.SelectToken(path).Replace(do_json_hijo);
            }

            // do_json.SelectToken("detalleDTE").Replace(docs);

            var x = JsonConvert.SerializeObject(do_json);
            return JsonConvert.SerializeObject(do_json); ;
        }
        #endregion

        #region UPDATES TABLAS
        public int update_estado(string connstr, string tablename, int id, string esquema, string col_stado, string valor_estado, string id_col, DailyLogFileWriter logfile, ILogger log, string funcion, string modulo)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_stado + " = " + "'" + valor_estado + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            log.LogInformation("Update a Ejecutar en " + funcion + " : " + sqlupdatequery);
            logfile.Log("Update a Ejecutar en " + funcion + " : " + sqlupdatequery, modulo);

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                    log.LogInformation("Update " + sqlupdatequery + "se ejecuto en satisfactoriamente en: " + funcion);
                    logfile.Log("Update " + sqlupdatequery + "se ejecuto en satisfactoriamente en: " + funcion, modulo);
                }
                else
                {
                    tran.Rollback();
                    log.LogInformation("Error Update: " + sqlupdatequery + "se ejecuto y se hizo rollback en: " + funcion);
                    logfile.Log("Error Update: " + sqlupdatequery + "se ejecuto y se hizo rollback en: " + funcion, modulo);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                log.LogInformation("Error Ocurrio una Exepcion en update en: " + funcion + ", exepcion: " + ex.ToString());
                logfile.Log("Error Ocurrio una Exepcion en update en: " + funcion + ", exepcion: " + ex.ToString(), modulo);
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }

            log.LogInformation("Respuesta de update en: " + funcion + ", resp: " + resp.ToString());
            logfile.Log("Respuesta de update en: " + funcion + ", resp: " + resp.ToString(), modulo);

            return resp;
        }

        public int update_FGENERACION(string connstr, string tablename, int id, string esquema, string id_col, DailyLogFileWriter logfile, ILogger log, string funcion, string modulo)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET FGENERACION = systimestamp";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            log.LogInformation("Update a Ejecutar en " + funcion + " : " + sqlupdatequery);
            logfile.Log("Update a Ejecutar en " + funcion + " : " + sqlupdatequery, modulo);

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                    log.LogInformation("Update " + sqlupdatequery + "se ejecuto en satisfactoriamente en: " + funcion);
                    logfile.Log("Update " + sqlupdatequery + "se ejecuto en satisfactoriamente en: " + funcion, modulo);
                }
                else
                {
                    tran.Rollback();
                    log.LogInformation("Error Update: " + sqlupdatequery + "se ejecuto y se hizo rollback en: " + funcion);
                    logfile.Log("Error Update: " + sqlupdatequery + "se ejecuto y se hizo rollback en: " + funcion, modulo);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                log.LogInformation("Error Ocurrio una Exepcion en update en: " + funcion + ", exepcion: " + ex.ToString());
                logfile.Log("Error Ocurrio una Exepcion en update en: " + funcion + ", exepcion: " + ex.ToString(), modulo);
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }

            log.LogInformation("Respuesta de update en: " + funcion + ", resp: " + resp.ToString());
            logfile.Log("Respuesta de update en: " + funcion + ", resp: " + resp.ToString(), modulo);

            return resp;
        }

        public int update_datos_enc_od(string connstr, string tablename, int id, string esquema, string id_col, string json)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET JSONENVIOBHAMH = " + "'" + json + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_datos_enc_invalidate(string connstr, string tablename, int id, string esquema, string id_col, string json)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET JSONENVIOANULACION  = " + "'" + json + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_datos_enc_resp_invalidate(string connstr, string tablename, int id, string esquema, string id_col, string json)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET RESPUESTAANULACIONMH   = " + "'" + json + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_estado_by_uid(string connstr, string tablename, string uid, string esquema, string col_stado, string valor_estado, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_stado + " = " + "'" + valor_estado + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + "'" + uid + "'";
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_sellomh(string connstr, string tablename, int id, string esquema, string col_sello, string valor_sello, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_sello + " = " + "'" + valor_sello + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_sellomh_by_uid(string connstr, string tablename, string uid, string esquema, string col_sello, string valor_sello, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_sello + " = " + "'" + valor_sello + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + "'" + uid + "'";
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_sellomh_invalida(string connstr, string tablename, int id, string esquema, string col_sello, string valor_sello, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_sello + " = " + "'" + valor_sello + "'";
            sqlupdatequery += ", FCHINVALIDACIONMH=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_mh_resp(string connstr, string tablename, int id, string esquema, string col_resp, string mhresp, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_resp + " = " + "'" + mhresp + "'";
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_mh_resp_by_uid(string connstr, string tablename, string uid, string esquema, string col_resp, string mhresp, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_resp + " = " + "'" + mhresp + "'";
            sqlupdatequery += " WHERE " + id_col + " = " + "'" + uid + "'";
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_reintentos(string connstr, string tablename, int id, string esquema, string col_reintento, string valor_reintento, string id_col)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE " + esquema + "." + tablename;
            sqlupdatequery += " SET " + col_reintento + " = " + valor_reintento;
            sqlupdatequery += ", FMODIFICACION=systimestamp";
            sqlupdatequery += " WHERE " + id_col + " = " + id;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_estado_valores_contingency(string connstr, string updatestr)
        {
            int resp = 0;
            string sqlupdatequery = updatestr;

            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int update_valor_contingency(string connstr, string tablename, string esquema, string valor)
        {
            int resp = 0;
            string sqlupdatequery = "UPDATE ";
            sqlupdatequery += esquema + "." + tablename;
            sqlupdatequery += " SET VAL=" + valor;
            sqlupdatequery += " WHERE NAME='CONTINGENCIAMH'";
            sqlupdatequery += " AND TYPE='CONFIGURACION'";
            sqlupdatequery += " AND ACTIVEFLG='Y'";

            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlupdatequery;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        #endregion

        #region servicio PBS
        public string verifica_servicio_reception(string url)
        {
            string resp = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    resp = reader.ReadToEnd();
                    return resp;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    resp = errorText;
                    // log errorText
                }
                throw;
            }

            return resp;
        }

        public string GetToken(string url, string jsonst)
        {

            string resp = string.Empty;
            string strResponseValue = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = jsonst.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(jsonst);
            requestWriter.Close();
            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                resp = responseReader.ReadToEnd();
                responseReader.Close();
            }
            catch (Exception e)
            {
                resp = e.Message.ToString();

            }
            return resp;

        }

        public object SendtoReception(object jsonDTE, string tken, string url, dynamic conf, string connstr, string esquema, ILogger log, DailyLogFileWriter logfile, string modulo)
        {
            var client = new HttpClient();
            var jdte = JsonConvert.SerializeObject(jsonDTE);

            log.LogInformation("url reception: " + url);
            logfile.Log("url reception: " + url, modulo);

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(jdte, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(url)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tken);

            var response = client.SendAsync(requestMessage).Result;

            log.LogInformation("url reception response: " + response.IsSuccessStatusCode.ToString());
            logfile.Log("url reception response: " + response.IsSuccessStatusCode.ToString(), modulo);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;

                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };


                //dynamic respjson = JObject.Parse(responseContent);
                //string fprocesamiento = respjson.identificacion.fecEmi;
                //string hprocesamiento = respjson.identificacion.horEmi;
                //string codgen = respjson.responseMH.codigoGeneracion;
                //dynamic Jresp = respjson.responseMH;
                //string digital = respjson.digital;
                //string factidcol = (string)conf["Col_IdDocEnc"];
                //string factidval = string.Empty;
                //string insersql = string.Empty;
                //string resp = JsonConvert.SerializeObject(Jresp);
                //string selectstr = "select " + (string)conf["Col_IdDocEnc"] + "," + (string)conf["Col_UidDocEnc"] + " FROM " + (string)conf["Tbl_DocEncabezado"] + " WHERE " + (string)conf["Col_UidDocEnc"] + "='" + codgen + "'";

                /*inserta token en tabla*/
                //DataTable dt = Getdt(connstr, selectstr);

                //if (dt.Rows.Count > 0)
                //{
                //    factidval = dt.Rows[0][factidcol].ToString();

                //    if(!string.IsNullOrEmpty(digital))
                //    {
                //        string fecha = "TO_DATE('" + fprocesamiento + ' ' + hprocesamiento + "'" + ", 'YYYY-MM-DD HH24:mi:ss')";

                //        int tamanio = digital.Length;
                //        //temp = digital.ToCharArray(1, 4000);

                //        insersql = "INSERT INTO BHDTE.FACT_DOC_PDF(IDFACTE,FGENERACION,DOCUMENTO,CODGEN,TIPORG) VALUES (:param1, SYSDATE, :param3,:param4,:param5)";
                //        // insersql += " VALUES " + "(" + factidval + "," + "'" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + "'," + "'" + pdfBytes.ToString() + "'" + ")";

                //        //insert_pdf64(connstr, insersql,factidval, fecha, digital, codgen,"F",log);
                //        //WritePdf(digital, codgen + ".pdf","/SEF/", log);

                //    }
                //}

                /*************************/

                int start = responseContent.IndexOf("responseMH") + 12;

                string uid = responseContent.Substring(responseContent.IndexOf("codigoGeneracion") + 20, 36);

                update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], uid, esquema, (string)conf["Col_resp"], JsonConvert.SerializeObject(responseContent), (string)conf["Col_UidDocEnc"]);

                string temp = responseContent.Substring(start);
                int end = temp.LastIndexOf("},");
                //string resp = temp.Substring(0, end+1);
                string resp = temp.Substring(0, end + 1);
                //var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(responseContent, options);
                var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(resp, options);

                // update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], codgen, esquema, (string)conf["Col_resp"], responseContent, (string)conf["Col_UidDocEnc"]);
                return postResponse;
            }
            else
            {
                log.LogInformation("Error: no Hubo Respuesta, Codigo Respuesta: " + response.StatusCode.ToString());
                logfile.Log("Error: no Hubo Respuesta, Codigo Respuesta: " + response.StatusCode.ToString(), modulo);
                return null;
            }

        }

        public object SendtoInvalidation(object jsonDTE, string tken, string url, dynamic conf, string connstr, string esquema, ILogger log, DailyLogFileWriter logfile, string modulo)
        {
            log.LogInformation("Inicio SendtoInvalidation");
            logfile.Log("Inicio SendtoInvalidation", modulo);
            var client = new HttpClient();
            var jdte = JsonConvert.SerializeObject(jsonDTE);

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(jdte, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(url)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tken);

            var response = client.SendAsync(requestMessage).Result;
            log.LogInformation("response");
            logfile.Log("response", modulo);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;

                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                dynamic respjson = JObject.Parse(responseContent);
                // string fprocesamiento = respjson.identificacion.fecEmi;
                //  string hprocesamiento = respjson.identificacion.horEmi;
                string codgen = respjson.responseMH.codigoGeneracion;
                dynamic Jresp = respjson.responseMH;
                string digital = respjson.digital;
                string factidcol = (string)conf["Col_IdDocEnc"];
                string factidval = string.Empty;
                string insersql = string.Empty;
                string resp = JsonConvert.SerializeObject(Jresp);
                string selectstr = "select " + (string)conf["Col_IdDocEnc"] + "," + (string)conf["Col_UidDocEnc"] + " FROM " + (string)conf["Tbl_DocEncabezado"] + " WHERE " + (string)conf["Col_UidDocEnc"] + "='" + codgen + "'";

                /*inserta token en tabla*/
                DataTable dt = Getdt(connstr, selectstr);

                if (dt.Rows.Count > 0)
                {
                    factidval = dt.Rows[0][factidcol].ToString();

                    if (!string.IsNullOrEmpty(digital))
                    {
                        //string fecha = "TO_DATE('" + fprocesamiento + ' ' + hprocesamiento + "'" + ", 'YYYY-MM-DD HH24:mi:ss')";

                        //int tamanio = digital.Length;
                        //temp = digital.ToCharArray(1, 4000);

                        insersql = "INSERT INTO BHDTE.FACT_DOC_PDF(IDFACTE,FGENERACION,DOCUMENTO,CODGEN,TIPORG) VALUES (:param1, SYSDATE, :param3,:param4,:param5)";
                        // insersql += " VALUES " + "(" + factidval + "," + "'" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + "'," + "'" + pdfBytes.ToString() + "'" + ")";

                        //insert_pdf64(connstr, insersql, factidval, "SYSDATE", digital, codgen, "I", log);
                        //WritePdf(digital, codgen + ".pdf", "/SEF/", log);

                    }
                }

                logfile.Log("responseContent", modulo);
                log.LogInformation("responseContent");
                //int start = responseContent.IndexOf("responseMH") + 12;

                //string uid = responseContent.Substring(responseContent.IndexOf("codigoGeneracion") + 19, 36);

                update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], codgen, esquema, (string)conf["Col_resp"], responseContent.ToString(), (string)conf["Col_UidDocEnc"]);

                //string temp = responseContent.Substring(start);
                //int end = temp.LastIndexOf("}");
                //string resp = temp.Substring(0, end);

                //log.LogInformation("resp; " + resp);
                //logfile.Log("resp; " + resp,modulo);
                //var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(responseContent, options);
                try
                {
                    var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(resp, options);
                    return postResponse;
                }
                catch (Exception ex)
                {
                    log.LogInformation("Error:" + responseContent);
                    return responseContent;
                }

            }
            else
            {
                log.LogInformation("Error: " + response.StatusCode.ToString());
                logfile.Log("Error: " + response.StatusCode.ToString(), modulo);
                return null;
            }
        }

        public object SendtocContingencyEvent(object jsonDTE, string tken, string url, dynamic conf, string connstr, string esquema, ILogger log)
        {
            var client = new HttpClient();
            var jdte = JsonConvert.SerializeObject(jsonDTE);

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(jdte, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(url)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tken);

            var response = client.SendAsync(requestMessage).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;

                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };


                int start = responseContent.IndexOf("responseMH") + 12;

                string uid = responseContent.Substring(responseContent.IndexOf("codigoGeneracion") + 20, 36);

                // update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], uid, esquema, (string)conf["Col_resp"], responseContent.ToString(), (string)conf["Col_UidDocEnc"]);

                string temp = responseContent.Substring(start);
                int end = temp.LastIndexOf("}}");
                string resp = temp.Substring(0, end + 1);
                //var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(responseContent, options);
                try
                {
                    var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(resp, options);
                    return postResponse;
                }
                catch (Exception ex)
                {
                    log.LogInformation("Error:" + responseContent);
                    return responseContent;
                }

            }
            else
            {
                log.LogInformation("Error: " + response.StatusCode.ToString());
                return null;
            }

        }

        public object SendtoRetrive(object jsonDTE, string tken, string url, dynamic conf, string connstr, string esquema, ILogger log, DailyLogFileWriter logfile, string modulo)
        {
            var client = new HttpClient();
            var jdte = JsonConvert.SerializeObject(jsonDTE);

            log.LogInformation("url retrive: " + url);
            logfile.Log("url retive: " + url, modulo);

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(jdte, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(url)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tken);

            var response = client.SendAsync(requestMessage).Result;

            log.LogInformation("url reception response: " + response.IsSuccessStatusCode.ToString());
            logfile.Log("url reception response: " + response.IsSuccessStatusCode.ToString(), modulo);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;

                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                //int start = responseContent.IndexOf("responseMH") + 12;

                //string uid = responseContent.Substring(responseContent.IndexOf("codigoGeneracion") + 20, 36);

                //update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], uid, esquema, (string)conf["Col_resp"], JsonConvert.SerializeObject(responseContent), (string)conf["Col_UidDocEnc"]);

                //string temp = responseContent.Substring(start);
                // int end = temp.LastIndexOf("},");
                //string resp = temp.Substring(0, end+1);
                // string resp = temp.Substring(0, end + 1);
                //var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(responseContent, options);
                var postResponse = System.Text.Json.JsonSerializer.Deserialize<SelfClienteResponse>(responseContent, options);

                //update_mh_resp_by_uid(connstr, (string)conf["Tbl_DocEncabezado"], uid, esquema, (string)conf["Col_resp"], postResponse, (string)conf["Col_UidDocEnc"]);

                return postResponse;


            }
            else
            {
                log.LogInformation("Error no Hubo Respuesta!!!!!!");
                logfile.Log("Error no Hubo Respuesta!!!!!!", modulo);
                return null;
            }

        }

        #endregion

        #region GENERA JSON
        public JObject GeneraNodoRootJsonDinamico(string connstr, string esquema, string tipo_dte, DataRow dr, dynamic config, string id_col, ILogger log, string col_totalgravado)
        {
            DataTable dt_root = new DataTable();
            string nodo_root = string.Empty;
            string nodo_root_val = string.Empty;
            string tipo = string.Empty;
            string json_str = string.Empty;
            string json_strh = string.Empty;
            dynamic json_root = new JObject();
            DataTable dt_datos = new DataTable();

            int idrow = 0;
            string tipodte = string.Empty;
            string id_nodo = string.Empty;
            string tipo_dato = string.Empty;
            DataTable dt_nodo = new DataTable();

            string nodo_root_id = string.Empty;

            string Sqlselectdoc_detalle = string.Empty;
            string Sqlselec_resumen = string.Empty;
            string Sqlselec_docrelacionado = string.Empty;
            string Sqlselect_otrosdocumentos = string.Empty;

            Sqlselectdoc_detalle = config["Sqlselectdoc_detalle"];
            Sqlselec_resumen = config["Sqlselec_resumen"];
            Sqlselec_docrelacionado = config["Sqlselec_docrelacionado"];
            Sqlselect_otrosdocumentos = config["Sqlselect_otrosdocumentos"];

            //log.LogInformation("**********************************GeneraNodoRootJsonDinamico*********************************");
            //log.LogInformation("Sqlselectdoc_detalle: " + Sqlselectdoc_detalle);
            //log.LogInformation("Sqlselec_resumen: " + Sqlselec_resumen);
            //log.LogInformation("Sqlselec_docrelacionado: " + Sqlselec_docrelacionado);
            //log.LogInformation("Sqlselect_otrosdocumentos: " + Sqlselect_otrosdocumentos);

            nodo_root_id = prop_valor(connstr, select_config_id_val(tipo_dte, "CATALOGO", "CAT-002", esquema));
            idrow = Convert.ToInt32(dr[id_col].ToString());
            //tipodte = dr["TIPD_INTERNO"].ToString();

            log.LogInformation("DOCUMENTO_ID: " + idrow);


            dt_root.Rows.Clear();
            dt_root.Columns.Clear();
            dt_root = Getdt(connstr, select_config_str("JSON", "root", nodo_root_id, esquema));
            if (dt_root.Rows.Count > 0)
            {
                for (int y = 0; y < dt_root.Rows.Count; y++)
                {
                    nodo_root_val = dt_root.Rows[y]["VAL"].ToString();
                    tipo_dato = dt_root.Rows[y]["DATATYPE"].ToString();
                    json_root.Add(nodo_root_val, null);
                    id_nodo = prop_valor(connstr, select_config_id_val_parent(nodo_root_val, "JSON", "root", esquema, nodo_root_id));
                    dt_nodo = Getdt(connstr, select_config_str("JSON", nodo_root_val, id_nodo, esquema));
                    var path = nodo_root_val;
                    //log.LogInformation("nodo_root_val: " + nodo_root_val);
                    //log.LogInformation("path: " + path);
                    if (dt_nodo.Rows.Count > 0)
                    {
                        switch (tipo_dato)
                        {
                            case "dt":
                                //dt_datos = Getdt(connstr, Sqlselec_resumen + idrow);
                                if (nodo_root_val == "resumen")
                                {
                                    log.LogInformation("Select resumen: " + Sqlselec_resumen + idrow);
                                    dt_datos = Getdt(connstr, Sqlselec_resumen + idrow);
                                    log.LogInformation("numero de registros resumen: " + dt_datos.Rows.Count.ToString());
                                }
                                else
                                {
                                    if (nodo_root_val == "cuerpoDocumento")
                                    {
                                        log.LogInformation("Select Detalle: " + Sqlselectdoc_detalle + idrow);
                                        dt_datos = Getdt(connstr, Sqlselectdoc_detalle + idrow);
                                        log.LogInformation("numero de registros cuerpoDocumento: " + dt_datos.Rows.Count.ToString());
                                    }
                                }
                                if (dt_datos.Rows.Count > 0)
                                {
                                    json_strh = json_nodos_hijos_tables(connstr, esquema, dt_datos, dt_nodo, tipo_dte, nodo_root_val, col_totalgravado).Replace("[{", "{").Replace("}]", "}");
                                    var obj1 = JObject.Parse(json_strh);
                                    json_root.SelectToken(path).Replace(obj1.SelectToken(path));
                                }
                                break;

                            case "dt[]":
                                if (nodo_root_val == "otrosDocumentos")
                                {
                                    log.LogInformation("Select otrosDocumentos: " + Sqlselect_otrosdocumentos + idrow);
                                    dt_datos = Getdt(connstr, Sqlselect_otrosdocumentos + idrow);
                                    log.LogInformation("numero de registros otrosDocumentos: " + dt_datos.Rows.Count.ToString());
                                }
                                else
                                {
                                    if (nodo_root_val == "documentoRelacionado")
                                    {
                                        log.LogInformation("Select documentoRelacionado: " + Sqlselec_docrelacionado + idrow);
                                        dt_datos = Getdt(connstr, Sqlselec_docrelacionado + idrow);
                                        log.LogInformation("numero de registros documentoRelacionado: " + dt_datos.Rows.Count.ToString());

                                    }
                                    else
                                    {
                                        log.LogInformation("Select Sqlselectdoc_detalle: " + Sqlselectdoc_detalle + idrow);
                                        dt_datos = Getdt(connstr, Sqlselectdoc_detalle + idrow);
                                    }
                                }

                                if (dt_datos.Rows.Count > 0)
                                {
                                    json_strh = json_nodos_hijos_tables(connstr, esquema, dt_datos, dt_nodo, tipo_dte, nodo_root_val, col_totalgravado);
                                    var odetalle = JObject.Parse(json_strh);
                                    json_root.SelectToken(path).Replace(odetalle.SelectToken(path));
                                    log.LogInformation("json_strh: " + json_strh);
                                }
                                else
                                {
                                    log.LogInformation("dt_datos: " + dt_datos.TableName.ToString());
                                }
                                break;

                            default:
                                json_str = GeneraCamposNodoDinamico(dt_nodo, dr, nodo_root_val, connstr, esquema, tipo_dte);
                                var o1 = JObject.Parse(json_str);

                                if (o1.Root.SelectToken(nodo_root_val).Path == "ventaTercero")
                                {
                                    json_root.SelectToken(path).Replace(o1.SelectToken(path));
                                }
                                else
                                {
                                    json_root.SelectToken(path).Replace(o1.SelectToken(path));
                                }

                                break;
                        }

                    }

                }
            }

            return json_root;
        }

        public string GeneraCamposNodoDinamico(DataTable dt, DataRow dr, string nodo_root, string connstr, string esquema, string tipo_dte)
        {
            string propiedad = string.Empty;
            string svalor = string.Empty;
            int nvalor = 0;
            string columna = string.Empty;
            string tipo = string.Empty;
            string escolumna = string.Empty;
            dynamic jsonObject = new JObject();
            string json_str_nodo = string.Empty;
            DataTable dt_nieto = new DataTable();
            string id_parent = string.Empty;
            StringBuilder st = new StringBuilder();

            if (dt.Rows.Count > 0)
            {
                for (int u = 0; u < dt.Rows.Count; u++)
                {
                    propiedad = dt.Rows[u]["NAME"].ToString(); //campo de json
                    columna = dt.Rows[u]["VAL"].ToString(); //donde se encuentra el valor en la tabla tercera
                    escolumna = dt.Rows[u]["ISCOLUMN"].ToString(); //si el valor de la propiedad
                    tipo = dt.Rows[u]["DATATYPE"].ToString(); //tipo de dato
                    //tipo_dte = dr["TIPD_INTERNO"].ToString(); //tipo de dato

                    if (string.IsNullOrEmpty(columna) || string.IsNullOrWhiteSpace(columna))
                    {
                        if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                        {
                            //switch (tipo)
                            //{
                            //    case "integer":
                            //            nvalor = Convert.ToInt16(prop_valor(connstr, select_config_valor(tipo_dte, "VERSION", "", esquema)));
                            //            jsonObject.Add(propiedad, nvalor);
                            //        break;

                            //    default:
                            //            svalor = prop_valor(connstr, select_config_valor(tipo_dte, "VERSION", "", esquema));
                            //            jsonObject.Add(propiedad, svalor);
                            //        break;
                            //}
                        }
                        else
                        {
                            svalor = "null";
                            jsonObject.Add(propiedad, svalor);
                        }
                    }
                    else
                    {
                        if (escolumna == "Y")
                        {
                            if (string.IsNullOrEmpty(dr[columna].ToString()))
                            {
                                svalor = null;
                                jsonObject.Add(propiedad, svalor);
                            }
                            else
                            {
                                switch (tipo)
                                {
                                    case "integer":
                                        nvalor = Convert.ToInt16(dr[columna].ToString());
                                        jsonObject.Add(propiedad, nvalor);
                                        break;

                                    case "date":
                                        svalor = dr[columna].ToString();
                                        DateTime d = Convert.ToDateTime(svalor);
                                        jsonObject.Add(propiedad, d.ToString("yyyy-MM-dd"));
                                        break;

                                    case "time":
                                        svalor = dr[columna].ToString();
                                        DateTime t = Convert.ToDateTime(svalor);
                                        jsonObject.Add(propiedad, t.ToString("HH:mm:ss"));
                                        break;

                                    default:
                                        svalor = dr[columna].ToString();
                                        jsonObject.Add(propiedad, svalor);
                                        break;

                                }

                            }
                        }
                        else
                        {
                            if (tipo == "[]")
                            {
                                id_parent = prop_valor(connstr, select_config_id_val_dtetype(propiedad, "JSON", nodo_root, esquema, tipo_dte));
                                dt_nieto = Getdt(connstr, select_config_str("JSON", propiedad, id_parent, esquema));
                                if (dt_nieto != null)
                                {
                                    st.Append("{");
                                    foreach (DataRow dr_nieto in dt_nieto.Rows)
                                    {
                                        svalor = dr[dr_nieto["VAL"].ToString()].ToString();
                                        st.Append("\"" + dr_nieto["NAME"].ToString() + "\"" + ":" + "\"" + svalor + "\"" + ",");
                                    }
                                    st.Remove(st.Length - 1, 1);
                                    st.Append("}");
                                }
                                svalor = st.ToString();
                                dynamic data = JObject.Parse(svalor);
                                jsonObject.Add(propiedad, data);
                            }
                            else
                            {
                                switch (tipo)
                                {
                                    case "integer":
                                        nvalor = Convert.ToInt16(columna);
                                        jsonObject.Add(propiedad, nvalor);
                                        break;

                                    default:
                                        svalor = columna;
                                        if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor) || svalor == "null")
                                        {
                                            jsonObject.Add(propiedad, null);
                                        }
                                        else
                                        {
                                            jsonObject.Add(propiedad, svalor);
                                        }

                                        break;

                                }

                            }
                        }
                    }
                }

            }
            json_str_nodo = "{" + "\"" + nodo_root + "\"" + ":" + jsonObject.ToString() + "}";
            return json_str_nodo;
        }

        public string json_nodos_hijos_tables(string connstr, string esquema, DataTable dtdatos, DataTable dtnodo, string tipo_dte, string nodo_root, string colventagravada)
        {

            string propiedad = string.Empty;
            string svalor = string.Empty;
            int nvalor = 0;
            string columna = string.Empty;
            string tipo = string.Empty;
            string escolumna = string.Empty;
            string svalor1 = string.Empty;
            decimal total_gravado = 0;
            DataTable dt_tributos = new DataTable();

            StringBuilder json_str_nodo = new StringBuilder();
            //string tipo_dte = string.Empty;

            if (dtnodo.Rows.Count > 0 && dtdatos.Rows.Count > 0)
            {
                json_str_nodo.Append("{");
                json_str_nodo.Append("\"" + nodo_root + "\"" + ":" + "[");

                foreach (DataRow dr in dtdatos.Rows)
                {
                    json_str_nodo.Append("{");
                    for (int u = 0; u < dtnodo.Rows.Count; u++)
                    {
                        propiedad = dtnodo.Rows[u]["NAME"].ToString(); //campo de json
                        columna = dtnodo.Rows[u]["VAL"].ToString(); //donde se encuentra el valor en la tabla tercera
                        escolumna = dtnodo.Rows[u]["ISCOLUMN"].ToString(); //si el valor de la propiedad
                        tipo = dtnodo.Rows[u]["DATATYPE"].ToString(); //tipo de dato
                                                                      //tipo_dte = dr["TIPD_INTERNO"].ToString(); //tipo de dato
                        if (string.IsNullOrEmpty(columna) || string.IsNullOrWhiteSpace(columna))
                        {
                            if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                            {
                                switch (tipo)
                                {
                                    case "integer":
                                        nvalor = Convert.ToInt16(prop_valor(connstr, select_config_valor(tipo_dte, "VERSION", "", esquema)));

                                        break;



                                    default:
                                        svalor = prop_valor(connstr, select_config_valor(tipo_dte, "VERSION", "", esquema));
                                        json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                                        break;
                                }
                            }
                            else
                            {
                                svalor = "null";
                                json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                            }
                        }
                        else
                        {
                            if (escolumna == "Y")
                            {
                                if (string.IsNullOrEmpty(dr[columna].ToString()))
                                {
                                    svalor = "null";
                                    json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                                }
                                else
                                {
                                    switch (tipo)
                                    {
                                        case "number":
                                            svalor = dr[columna].ToString();
                                            json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                                            break;

                                        case "date":
                                            DateTime dt = new DateTime();
                                            dt = Convert.ToDateTime(dr[columna].ToString());
                                            svalor = dt.ToString("yyyy-MM-dd");
                                            json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "\"" + svalor + "\"" + ",");
                                            break;

                                        case "{}":
                                            svalor1 = "";
                                            svalor = dr[columna].ToString();

                                            string[] temp = new string[10];
                                            temp = svalor.Split(",");
                                            for (int v = 0; v < temp.Length; v++)
                                            {
                                                svalor1 += "'" + temp[v] + "',";
                                            }
                                            svalor = svalor1.Remove(svalor1.Length - 1);
                                            if (nodo_root == "cuerpoDocumento" && propiedad == "tributos")
                                            {
                                                /*decimal VNOSUJETAS = 0;
                                                decimal VEXENTAS = 0;
                                                decimal VGRAVADAS = 0;
                                                VNOSUJETAS=Convert.ToDecimal(dr["VNOSUJETAS"].ToString());
                                                VEXENTAS = Convert.ToDecimal(dr["VEXENTAS"].ToString());
                                                VGRAVADAS = Convert.ToDecimal(dr["VGRAVADAS"].ToString());
                                                if(VNOSUJETAS==0 && VEXENTAS==0 && VGRAVADAS>0)*/
                                                //{
                                                svalor = svalor1.Remove(svalor1.Length - 1);
                                                json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "[" + svalor + "]" + ",");
                                                // }
                                                /* else
                                                 {
                                                     svalor = svalor1.Remove(svalor1.Length - 1);
                                                     json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "null" + ",");
                                                 }*/
                                            }
                                            else
                                            {
                                                if (nodo_root == "resumen" && propiedad == "tributos")
                                                {
                                                    total_gravado = Convert.ToDecimal(dr[colventagravada].ToString());
                                                    dt_tributos = Getdt(connstr, select_tributos("SEFCLIENT_CONF", esquema, svalor, "RESUMEN DEL DTE"));
                                                    if (dt_tributos.Rows.Count > 0)
                                                    {
                                                        svalor = json_tributos(dt_tributos, total_gravado);
                                                        json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "[" + svalor + "]" + ",");
                                                    }
                                                    else
                                                    {
                                                        svalor = "null";
                                                        json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                                                    }
                                                }
                                            }
                                            break;


                                        default:
                                            svalor = dr[columna].ToString();
                                            json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "\"" + svalor + "\"" + ",");
                                            break;
                                    }

                                }
                            }
                            else
                            {
                                switch (tipo)
                                {
                                    case "{}":
                                        svalor = columna;
                                        string[] temp = new string[10];
                                        temp = columna.Split(",");
                                        for (int v = 0; v < temp.Length; v++)
                                        {
                                            svalor = "\"" + temp[v] + "\",";
                                        }
                                        svalor = svalor.Remove(svalor.Length - 1);
                                        json_str_nodo.Append("\"" + propiedad + "\"" + ":" + "[" + svalor + "]" + ",");
                                        break;

                                    default:
                                        svalor = columna;
                                        json_str_nodo.Append("\"" + propiedad + "\"" + ":" + svalor + ",");
                                        break;
                                }

                            }
                        }
                    }
                    json_str_nodo.Remove(json_str_nodo.Length - 1, 1);
                    json_str_nodo.Append("},");
                }
            }
            json_str_nodo.Remove(json_str_nodo.Length - 1, 1);
            json_str_nodo.Append("]");
            json_str_nodo.Append("}");
            return json_str_nodo.ToString();
        }

        public string json_tributos(DataTable dt, decimal subtotal)
        {

            dynamic do_jarray = new JArray();

            foreach (DataRow row in dt.Rows)
            {
                dynamic do_json = new JObject();
                do_json.Add("codigo", row["NAME"].ToString());
                do_json.Add("descripcion", row["DESCTEXT"].ToString());
                decimal iva = Convert.ToDecimal(row["VAL"].ToString()) * subtotal;
                do_json.Add("valor", Math.Round(iva, 2));
                do_jarray.Add(do_json);
            }



            return JsonConvert.SerializeObject(do_jarray); ;
        }

        public string fix_json(dynamic doc, ILogger log, string tipoDte)
        {
            string tipodoc = string.Empty;
            string dui = string.Empty;
            string tipodte = string.Empty;
            JObject docJObject = JObject.Parse(doc);
            string depto = string.Empty;
            string muni = string.Empty;
            string complemento = string.Empty;
            string nitt = string.Empty;
            string nombret = string.Empty;
            DataTable dt_apendices = new DataTable();

            if (tipoDte == "01" || tipoDte == "11")
            {
                if (string.IsNullOrEmpty((string)docJObject.SelectToken("receptor.tipoDocumento")) && string.IsNullOrEmpty((string)docJObject.SelectToken("receptor.numDocumento")))
                {

                }
                else
                {
                    tipodoc = (string)docJObject.SelectToken("receptor.tipoDocumento");
                    dui = (string)docJObject.SelectToken("receptor.numDocumento");
                    int tamanio_dui = 0;
                    tamanio_dui = dui.Length;

                    string ndui = dui.Substring(0, tamanio_dui - 1);
                    string ultimo_caracter = dui.Substring(tamanio_dui - 1);

                    log.LogInformation("Tipo de Documento: " + tipodoc);
                    log.LogInformation("Dui Sin Formato: " + dui);
                    log.LogInformation("Dui: " + ndui);

                    if (Convert.ToInt16(tipodoc) == 13)
                    {
                        JObject numdoc = new JObject();

                        string temp = ndui + "-" + ultimo_caracter;
                        numdoc.Add("numDocumento", temp);
                        docJObject.SelectToken("receptor.numDocumento").Replace(numdoc.SelectToken("numDocumento"));
                        log.LogInformation("Dui Con Formato: " + temp);
                    }



                }

                if (tipoDte == "01")
                {
                    depto = (string)docJObject.SelectToken("receptor.direccion.departamento");
                    muni = (string)docJObject.SelectToken("receptor.direccion.municipio");
                    complemento = (string)docJObject.SelectToken("receptor.direccion.complemento");

                    if (string.IsNullOrEmpty(depto) && string.IsNullOrEmpty(muni) && string.IsNullOrEmpty(complemento))
                    {
                        JObject dir = new JObject();
                        dir.Add("direccion", null);
                        docJObject.SelectToken("receptor.direccion").Replace(dir.SelectToken("direccion"));
                    }
                }
            }


            if (tipoDte == "07" || tipoDte == "08" || tipoDte == "09" || tipoDte == "14")
            {

            }
            else
            {
                nitt = (string)docJObject.SelectToken("ventaTercero.nit");
                nombret = (string)docJObject.SelectToken("ventaTercero.nombre");

                if (string.IsNullOrEmpty(nitt) && string.IsNullOrEmpty(nombret))
                {
                    JObject vt = new JObject();
                    vt.Add("ventaTercero", null);
                    docJObject.SelectToken("ventaTercero").Replace(vt.SelectToken("ventaTercero"));
                }
            }


            //JObject receptorJson = docJObject["receptor"];

            //log.LogInformation("Dui Sin Formato: " + dui);
            //dynamic contactExists = JsonConvert.DeserializeObject(docJObject);
            // var tipodoc = receptorJson.SelectToken("tipoDocumento");
            //var tipodoc = docJObject["receptor"]["tipoDocumento"];
            //_ = (string)dataJson["tipoDocumento"];
            //dui = (string)dataJson["numDocumento"];



            return JsonConvert.SerializeObject(docJObject);

        }

        public string fix_json_invalidate(dynamic doc, ILogger log, string tipoDte)
        {
            string tipodoc = string.Empty;
            string dui = string.Empty;
            string tipodte = string.Empty;
            JObject docJObject = JObject.Parse(doc);
            string depto = string.Empty;
            string muni = string.Empty;
            string complemento = string.Empty;
            string nitt = string.Empty;
            string nombret = string.Empty;

            if (tipoDte == "01" || tipoDte == "11")
            {
                if (string.IsNullOrEmpty((string)docJObject.SelectToken("documento.tipoDocumento")) && string.IsNullOrEmpty((string)docJObject.SelectToken("documento.numDocumento")))
                {

                }
                else
                {
                    tipodoc = (string)docJObject.SelectToken("documento.tipoDocumento");
                    dui = (string)docJObject.SelectToken("documento.numDocumento");
                    int tamanio_dui = 0;
                    tamanio_dui = dui.Length;

                    string ndui = dui.Substring(0, tamanio_dui - 1);
                    string ultimo_caracter = dui.Substring(tamanio_dui - 1);

                    log.LogInformation("Tipo de Documento: " + tipodoc);
                    log.LogInformation("Dui Sin Formato: " + dui);
                    log.LogInformation("Dui: " + ndui);

                    if (Convert.ToInt16(tipodoc) == 13)
                    {
                        JObject numdoc = new JObject();

                        string temp = ndui + "-" + ultimo_caracter;
                        numdoc.Add("numDocumento", temp);
                        docJObject.SelectToken("documento.numDocumento").Replace(numdoc.SelectToken("numDocumento"));
                        log.LogInformation("Dui Con Formato: " + temp);
                    }



                }

                //depto = (string)docJObject.SelectToken("receptor.direccion.departamento");
                //muni = (string)docJObject.SelectToken("receptor.direccion.municipio");
                //complemento = (string)docJObject.SelectToken("receptor.direccion.complemento");

                //if (string.IsNullOrEmpty(depto) && string.IsNullOrEmpty(muni) && string.IsNullOrEmpty(complemento))
                //{
                //    JObject dir = new JObject();
                //    dir.Add("direccion", null);
                //    docJObject.SelectToken("receptor.direccion").Replace(dir.SelectToken("direccion"));
                //}
            }


            //if (tipoDte == "07" || tipoDte == "08" || tipoDte == "09" || tipoDte == "14")
            //{

            //}
            //else
            //{
            //    nitt = (string)docJObject.SelectToken("ventaTercero.nit");
            //    nombret = (string)docJObject.SelectToken("ventaTercero.nombre");

            //    if (string.IsNullOrEmpty(nitt) && string.IsNullOrEmpty(nombret))
            //    {
            //        JObject vt = new JObject();
            //        vt.Add("ventaTercero", null);
            //        docJObject.SelectToken("ventaTercero").Replace(vt.SelectToken("ventaTercero"));
            //    }
            //}


            //JObject receptorJson = docJObject["receptor"];

            //log.LogInformation("Dui Sin Formato: " + dui);
            //dynamic contactExists = JsonConvert.DeserializeObject(docJObject);
            // //var tipodoc = receptorJson.SelectToken("tipoDocumento");
            //var tipodoc = docJObject["receptor"]["tipoDocumento"];
            ////_ = (string)dataJson["tipoDocumento"];
            //dui = (string)dataJson["numDocumento"];



            return JsonConvert.SerializeObject(docJObject);

        }


        #endregion

        #region configuraciones
        public DataTable Get_configuraciones(string connstr, string esquema)
        {
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            string sql_select = string.Empty;
            sql_select = "SELECT NAME, VAL ";
            sql_select += " FROM " + esquema + ".SEFCLIENT_CONF";
            sql_select += " WHERE TYPE = " + "'" + "CONFIGURACION" + "'";
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sql_select;
                cmd.CommandType = System.Data.CommandType.Text;
                da.Fill(dt);
                conn.Close();
            }
            catch
            {

            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                da.Dispose();
                cmd.Dispose();
                conn.Dispose();
            }
            return dt;
        }

        #endregion

        #region logs
        public int insert_logtbl(string connstr, string tablename, string esquema, string tipo, string mensaje, string id, string usuarioc, string usuariom)
        {
            int resp = 0;
            string sql_insert = string.Empty;

            sql_insert = "INSERT INTO " + esquema + "." + tablename;
            sql_insert += " (";
            sql_insert += " idfacte,";
            sql_insert += " fechacreacionlog,";
            sql_insert += " fechamodificacionlog,";
            sql_insert += " usuariocreacion,";
            sql_insert += " usuariomodificacion,";
            sql_insert += " tipo,";
            sql_insert += " log";
            sql_insert += " )";
            sql_insert += " VALUES";
            sql_insert += " (";
            if (string.IsNullOrEmpty(id))
            {
                sql_insert += "null" + ",";
            }
            else
            {
                sql_insert += id + ",";
            }
            sql_insert += " SYSDATE,";
            sql_insert += " SYSDATE,";
            if (string.IsNullOrEmpty(usuarioc))
            {
                sql_insert += "'" + "null" + "',";
            }
            else
            {
                sql_insert += "'" + usuarioc + "',";
            }
            if (string.IsNullOrEmpty(usuariom))
            {
                sql_insert += "'" + "null" + "',";
            }
            else
            {
                sql_insert += "'" + usuariom + "',";
            }
            sql_insert += "'" + tipo + "',";
            sql_insert += "'" + mensaje + "'";
            sql_insert += " )";

            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql_insert;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }

        public int insert_Arraylogtbl(string connstr, string tablename, string esquema, string tipo, string[] mensaje, string id, string usuarioc, string usuariom)
        {
            int resp = 0;
            string sql_insert = string.Empty;

            sql_insert = "INSERT INTO " + esquema + "." + tablename;
            sql_insert += " (";
            sql_insert += " id_fact_e,";
            sql_insert += " fecha_creacion_log,";
            sql_insert += " fecha_modificacion_log,";
            sql_insert += " usuario_creacion,";
            sql_insert += " usuario_modificacion,";
            sql_insert += " tipo,";
            sql_insert += " log";
            sql_insert += " )";
            sql_insert += " VALUES";
            sql_insert += " (";
            if (string.IsNullOrEmpty(id))
            {
                sql_insert += "null" + ",";
            }
            else
            {
                sql_insert += id + ",";
            }
            sql_insert += " SYSDATE,";
            sql_insert += " SYSDATE,";
            if (string.IsNullOrEmpty(usuarioc))
            {
                sql_insert += "'" + "null" + "',";
            }
            else
            {
                sql_insert += "'" + usuarioc + "',";
            }
            if (string.IsNullOrEmpty(usuariom))
            {
                sql_insert += "'" + "null" + "',";
            }
            else
            {
                sql_insert += "'" + usuariom + "',";
            }
            sql_insert += "'" + tipo + "',";
            sql_insert += "'" + mensaje + "'";
            sql_insert += " )";

            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleTransaction tran;

            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql_insert;
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }


            return resp;
        }
        #endregion

        #region insert_pdf
        public int insert_pdf64(string constr, string insertstr, string idfacte, string fgeneracion, string pdf, string codgen, string tiporg, ILogger log)
        {

            int resp = 0;
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(constr);
            OracleTransaction tran;


            OracleParameter param1 = new OracleParameter();
            param1.OracleDbType = OracleDbType.Int32;
            param1.ParameterName = "IDFACTE";
            param1.Value = Convert.ToInt32(idfacte);

            /*OracleParameter param2 = new OracleParameter();
            param2.OracleDbType = OracleDbType.TimeStamp;
            param2.ParameterName = "FGENERACION";
            param2.Value = fgeneracion;*/

            OracleParameter param3 = new OracleParameter();
            param3.OracleDbType = OracleDbType.Clob;
            param3.ParameterName = "DOCUMENTO";
            param3.Value = pdf;

            OracleParameter param4 = new OracleParameter();
            param4.OracleDbType = OracleDbType.Clob;
            param4.ParameterName = "CODGEN";
            param4.Value = codgen;

            OracleParameter param5 = new OracleParameter();
            param5.OracleDbType = OracleDbType.Clob;
            param5.ParameterName = "TIPORG";
            param5.Value = tiporg;



            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = insertstr;
                cmd.Parameters.Add(param1);
                // cmd.Parameters.Add(param2);
                cmd.Parameters.Add(param3);
                cmd.Parameters.Add(param4);
                cmd.Parameters.Add(param5);
                cmd.Transaction = tran;
                resp = cmd.ExecuteNonQuery();

                if (resp > 0)
                {
                    log.LogInformation("Info:  " + "se inserto el registro con IDFACTE: " + idfacte + " en la tabla");
                    tran.Commit();
                }
                else
                {
                    tran.Rollback();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                log.LogError("Error; " + ex.Message.ToString());
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                resp = -1;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                tran = null;
                cmd.Dispose();
                conn.Dispose();
            }

            return resp;
        }


        #endregion

        #region basededatos_test_conexion
        public string OracleConnStr(string host, string port, string service, string userid, string passwd)
        {
            string connstr = string.Empty;
            connstr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)";
            connstr += "(HOST = " + host + ")(PORT = " + port + ")))(CONNECT_DATA =(SERVICE_NAME = ";
            connstr += service + ")));User ID=" + userid + ";Password=" + passwd + "; Pooling=False;";

            return connstr;
        }

        public string TestConexionDB(string connstr)
        {
            DataTable dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(connstr);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            string resp = string.Empty;
            try
            {
                conn.Open();
                resp = "Conexion Establecida Correctamente !!!!!";
                conn.Close();
            }
            catch (Exception ex)
            {
                resp = "Error: " + ex.ToString();
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                {
                    conn.Close();
                }
                conn.Dispose();
            }
            return resp;
        }
        #endregion

        #region contingency
        public string json_contingency_event(DataTable dt, DataTable dt_val, JArray docs)
        {
            DataTable dt_filter = new DataTable();
            DataTable dt_filter_hijo = new DataTable();
            DataView dv_root = new DataView(dt);
            DataView dv_hijo = new DataView(dt);
            dv_root.RowFilter = "SUBTYPE='root'";
            dt_filter = dv_root.ToTable();
            string datatype = string.Empty;
            int nvalor = 0;
            string svalor = string.Empty;
            string propiedad = string.Empty;
            string iscolumn = string.Empty;
            string columna = string.Empty;
            dynamic do_jarray = new JArray();
            dynamic do_json = new JObject();

            foreach (DataRow row in dt_filter.Rows)
            {
                dynamic do_json_hijo = new JObject();
                var path = row["VAL"].ToString();
                do_json.Add(path, null);

                dv_hijo.RowFilter = "SUBTYPE=" + "'" + row["VAL"].ToString() + "'";
                dt_filter_hijo = dv_hijo.ToTable();
                foreach (DataRow row1 in dt_filter_hijo.Rows)
                {
                    datatype = row1["DATATYPE"].ToString();
                    propiedad = row1["NAME"].ToString();
                    svalor = row1["VAL"].ToString();
                    columna = row1["VAL"].ToString();
                    iscolumn = row1["ISCOLUMN"].ToString();

                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                    {
                        do_json_hijo.Add(propiedad, "null");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(iscolumn) || string.IsNullOrWhiteSpace(iscolumn))
                        {
                            switch (datatype)
                            {
                                case "number":
                                    nvalor = Convert.ToInt16(svalor);
                                    do_json_hijo.Add(propiedad, nvalor);
                                    break;

                                case "string":
                                    do_json_hijo.Add(propiedad, svalor);
                                    break;
                            }
                        }
                        else
                        {
                            switch (datatype)
                            {
                                case "number":
                                    svalor = dt_val.Rows[0][columna].ToString();
                                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                                    {
                                        nvalor = Convert.ToInt16("0");
                                        do_json_hijo.Add(propiedad, nvalor);
                                    }
                                    else
                                    {
                                        nvalor = Convert.ToInt16(svalor);
                                        do_json_hijo.Add(propiedad, nvalor);
                                    }

                                    break;

                                case "string":
                                    svalor = dt_val.Rows[0][columna].ToString();
                                    if (string.IsNullOrEmpty(svalor) || string.IsNullOrWhiteSpace(svalor))
                                    {
                                        do_json_hijo.Add(propiedad, null);
                                    }
                                    else
                                    {
                                        if (propiedad == "codigoGeneracion")
                                        {
                                            var guid = Guid.NewGuid().ToString().ToUpper();
                                            do_json_hijo.Add(propiedad, guid);
                                        }
                                        else
                                        {
                                            do_json_hijo.Add(propiedad, svalor);
                                        }
                                    }

                                    break;
                                default:
                                    svalor = "null";
                                    do_json_hijo.Add(propiedad, svalor);
                                    break;
                            }
                        }
                    }

                }
                do_json.SelectToken(path).Replace(do_json_hijo);
            }

            do_json.SelectToken("detalleDTE").Replace(docs);

            var x = JsonConvert.SerializeObject(do_json);

            return JsonConvert.SerializeObject(do_json); ;
        }
        #endregion

        #region cache
        public string ObtenerOCrearCache(IMemoryCache cache, string key, string urlsecurity, string urlsecuritybody, ILogger log, DailyLogFileWriter logfile, string funcion, string modulo)
        {
            string clave = key;
            string valor;

            if (!cache.TryGetValue(clave, out valor))
            {
                var opcionesCache = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(2));

                opcionesCache.RegisterPostEvictionCallback((key, value, reason, state) =>
                {
                    log.LogInformation($"La entrada con clave '{key}' expiro. Razon: {reason}");
                    logfile.Log($"La entrada con clave '{key}' expiro. Razon: {reason}", modulo);
                });

                valor = GetToken(urlsecurity, urlsecuritybody);

                cache.Set(clave, valor, opcionesCache);

                log.LogInformation("token: " + valor + " Creado a las: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss") + " en " + funcion);
                logfile.Log("token: " + valor + " Creado a las: " + DateTimeOffset.Now.ToString("yyyy-MM-dd HH:mm:ss") + " en " + funcion, modulo);
            }
            else
            {
                valor = cache.Get(clave).ToString();
                log.LogInformation(clave + " en cache con valor " + " : " + valor + " en " + funcion);
                logfile.Log(clave + " en cache con valor " + " : " + valor + " en " + funcion, modulo);
            }


            return valor;

        }
        #endregion

        #region validadocrelacionado

        public string ValidaSelloDocRelacionado(string connstr, string esquema, string collsello, string tablaenc, string colcodgen, string jsonaenviar)
        {
            string sello = string.Empty;
            string num_doc_relacionado = string.Empty;
            string resp = "procesar";

            try
            {
                dynamic data1 = JObject.Parse(jsonaenviar);

                JArray documentos = (JArray)data1["cuerpoDocumento"];
                foreach (JToken documento in documentos)
                {
                    num_doc_relacionado = (string)documento["numeroDocumento"];
                    if (!string.IsNullOrEmpty(num_doc_relacionado))
                    {
                        sello = GetSello(connstr, esquema, collsello, tablaenc, colcodgen, num_doc_relacionado);
                        if (string.IsNullOrEmpty(sello))
                        {
                            resp = "No procesar";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                num_doc_relacionado = string.Empty;
            }

            return resp;
        }

        #endregion

        #region writePDF
        public void WritePdf(string pdfbase64, string filename, string directorio, ILogger log)
        {


            string path = directorio; // Ruta al directorio donde se guardará el PDF

            // Decodificar el PDF base64 a bytes
            byte[] pdfBytes = Convert.FromBase64String(pdfbase64);


            try
            {
                var dia = DateTime.Now.ToString("dd");
                var mes = DateTime.Now.ToString("MM");
                var anio = DateTime.Now.ToString("yyyy");

                string directorioDestino = $"{path}/{anio}/{mes}/{dia}";
                log.LogInformation("directorioDestino: " + directorioDestino);
                // Verificar si el directorio existe, si no existe, crearlo
                if (!Directory.Exists(directorioDestino))
                {
                    Directory.CreateDirectory(directorioDestino);

                    // Dar permisos al directorio para escribir
                    DirectoryInfo directorioInfo = new DirectoryInfo(directorioDestino);
                    /*var permisos = new System.Security.AccessControl.DirectorySecurity();
                    permisos.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(usuario, System.Security.AccessControl.FileSystemRights.Write, System.Security.AccessControl.AccessControlType.Allow));
                    directorioInfo.SetAccessControl(permisos);*/
                }

                string rutaArchivoPDF = Path.Combine(directorioDestino, filename);
                //File.WriteAllBytes(rutaArchivoPDF, pdfBytes);

                FileStream streamForPdf = new FileStream(rutaArchivoPDF, FileMode.CreateNew);
                BinaryWriter writerPdf = new BinaryWriter(streamForPdf);
                writerPdf.Write(pdfBytes, 0, pdfBytes.Length);
                writerPdf.Close();
                writerPdf.Dispose();

                log.LogInformation("PDF guardado exitosamente en: " + rutaArchivoPDF);


            }
            catch (Exception ex)
            {

                log.LogError(ex.Message.ToString());
            }


        }
        #endregion
    }
}
