﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.WindowsService.Services
{
    public static class PivoteDataTable
    {
        public static DataTable Pivot(this DataTable tbl)
        {
            var tblPivot = new DataTable();
            //tblPivot.Columns.Add(tbl.Columns[0].ColumnName);
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                tblPivot.Columns.Add((string?)tbl.Rows[i]["NAME"]);
            }
            for (int col = 1; col < tbl.Columns.Count; col++)
            {
                var r = tblPivot.NewRow();
                //r[0] = tbl.Columns[col].ToString();
                for (int j = 0; j < tbl.Rows.Count; j++)
                    r[j] = tbl.Rows[j][col];

                tblPivot.Rows.Add(r);
            }
            return tblPivot;
        }
    }
}
